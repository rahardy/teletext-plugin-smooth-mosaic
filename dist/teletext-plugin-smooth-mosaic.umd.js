// @preserve SPDX-FileCopyrightText: (c) 2021 Tech and Software Ltd.
// @preserve SPDX-FileCopyrightText: Copyright (C) 2017 Rohan Deshpande ( rohan@creativelifeform.com )
// @preserve SPDX-FileCopyrightText: Copyright (C) 2010 Dominic Szablewski ( mail@phoboslab.org )
// @preserve SPDX-FileCopyrightText: Copyright (C) 2010 Cameron Zemek ( grom@zeminvaders.net )
// @preserve SPDX-FileCopyrightText: Copyright (C) 2003 Maxim Stepin ( maxst@hiend3d.com )
// @preserve SPDX-License-Identifier: LGPL-2.1-or-later
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.ttxsmooth = {}));
}(this, (function (exports) { 'use strict';

	var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function getDefaultExportFromCjs (x) {
		return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
	}

	var hqx$1 = {exports: {}};

	(function (module, exports) {
	(function webpackUniversalModuleDefinition(root, factory) {
		module.exports = factory();
	})(commonjsGlobal, function() {
	return /******/ (function(modules) { // webpackBootstrap
	/******/ 	// The module cache
	/******/ 	var installedModules = {};
	/******/
	/******/ 	// The require function
	/******/ 	function __webpack_require__(moduleId) {
	/******/
	/******/ 		// Check if module is in cache
	/******/ 		if(installedModules[moduleId]) {
	/******/ 			return installedModules[moduleId].exports;
	/******/ 		}
	/******/ 		// Create a new module (and put it into the cache)
	/******/ 		var module = installedModules[moduleId] = {
	/******/ 			i: moduleId,
	/******/ 			l: false,
	/******/ 			exports: {}
	/******/ 		};
	/******/
	/******/ 		// Execute the module function
	/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
	/******/
	/******/ 		// Flag the module as loaded
	/******/ 		module.l = true;
	/******/
	/******/ 		// Return the exports of the module
	/******/ 		return module.exports;
	/******/ 	}
	/******/
	/******/
	/******/ 	// expose the modules object (__webpack_modules__)
	/******/ 	__webpack_require__.m = modules;
	/******/
	/******/ 	// expose the module cache
	/******/ 	__webpack_require__.c = installedModules;
	/******/
	/******/ 	// identity function for calling harmony imports with the correct context
	/******/ 	__webpack_require__.i = function(value) { return value; };
	/******/
	/******/ 	// define getter function for harmony exports
	/******/ 	__webpack_require__.d = function(exports, name, getter) {
	/******/ 		if(!__webpack_require__.o(exports, name)) {
	/******/ 			Object.defineProperty(exports, name, {
	/******/ 				configurable: false,
	/******/ 				enumerable: true,
	/******/ 				get: getter
	/******/ 			});
	/******/ 		}
	/******/ 	};
	/******/
	/******/ 	// getDefaultExport function for compatibility with non-harmony modules
	/******/ 	__webpack_require__.n = function(module) {
	/******/ 		var getter = module && module.__esModule ?
	/******/ 			function getDefault() { return module['default']; } :
	/******/ 			function getModuleExports() { return module; };
	/******/ 		__webpack_require__.d(getter, 'a', getter);
	/******/ 		return getter;
	/******/ 	};
	/******/
	/******/ 	// Object.prototype.hasOwnProperty.call
	/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
	/******/
	/******/ 	// __webpack_public_path__
	/******/ 	__webpack_require__.p = "";
	/******/
	/******/ 	// Load entry module and return exports
	/******/ 	return __webpack_require__(__webpack_require__.s = 0);
	/******/ })
	/************************************************************************/
	/******/ ([
	/* 0 */
	/***/ (function(module, exports, __webpack_require__) {


	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	exports.default = function (img, scale) {
	  // We can only scale with a factor of 2, 3 or 4
	  if ([2, 3, 4].indexOf(scale) === -1) {
	    return img;
	  }

	  var orig, origCtx, scaled, origPixels;
	  if (img instanceof HTMLCanvasElement) {
	    orig = img;
	    origCtx = orig.getContext('2d');
	    scaled = orig;
	    origPixels = origCtx.getImageData(0, 0, orig.width, orig.height).data;
	  } else {
	    origPixels = getImagePixels(img, 0, 0, img.width, img.height).data;
	    scaled = document.createElement('canvas');
	  }

	  // pack RGBA colors into integers
	  var count = img.width * img.height;
	  var src = _src = new Array(count);
	  var dest = _dest = new Array(count * scale * scale);
	  var index;
	  for (var i = 0; i < count; i++) {
	    src[i] = (origPixels[(index = i << 2) + 3] << 24) + (origPixels[index + 2] << 16) + (origPixels[index + 1] << 8) + origPixels[index];
	  }

	  // This is where the magic happens
	  if (scale === 2) hq2x(img.width, img.height);else if (scale === 3) hq3x(img.width, img.height);else if (scale === 4) hq4x(img.width, img.height);
	  // alternative: window['hq'+scale+'x']( img.width, img.height );

	  scaled.width = img.width * scale;
	  scaled.height = img.height * scale;

	  var scaledCtx = scaled.getContext('2d');
	  var scaledPixels = scaledCtx.getImageData(0, 0, scaled.width, scaled.height);
	  var scaledPixelsData = scaledPixels.data;

	  // unpack integers to RGBA
	  var c,
	      a,
	      destLength = dest.length;
	  for (var j = 0; j < destLength; j++) {
	    a = ((c = dest[j]) & 0xFF000000) >> 24;
	    scaledPixelsData[(index = j << 2) + 3] = a < 0 ? a + 256 : 0; // signed/unsigned :/
	    scaledPixelsData[index + 2] = (c & 0x00FF0000) >> 16;
	    scaledPixelsData[index + 1] = (c & 0x0000FF00) >> 8;
	    scaledPixelsData[index] = c & 0x000000FF;
	  }
	  _src = src = null;
	  _dest = dest = null;
	  scaledCtx.putImageData(scaledPixels, 0, 0);
	  return scaled;
	};

	// ==ClosureCompiler==
	// @compilation_level SIMPLE_OPTIMIZATIONS
	// ==/ClosureCompiler==

	/*
	 * Copyright (C) 2003 Maxim Stepin ( maxst@hiend3d.com )
	 *
	 * Copyright (C) 2010 Cameron Zemek ( grom@zeminvaders.net )
	 *
	 * Copyright (C) 2010 Dominic Szablewski ( mail@phoboslab.org )
	 *
	 * This program is free software; you can redistribute it and/or
	 * modify it under the terms of the GNU Lesser General Public
	 * License as published by the Free Software Foundation; either
	 * version 2.1 of the License, or (at your option) any later version.
	 *
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	 * Lesser General Public License for more details.
	 *
	 * You should have received a copy of the GNU Lesser General Public
	 * License along with this program; if not, write to the Free Software
	 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
	 */

	var _src = null,
	    _dest = null,
	    _MASK_2 = 0x00FF00,
	    _MASK_13 = 0xFF00FF,
	    _Ymask = 0x00FF0000,
	    _Umask = 0x0000FF00,
	    _Vmask = 0x000000FF,
	    _trY = 0x00300000,
	    _trU = 0x00000700,
	    _trV = 0x00000006;

	var _Math = Math; // global to local. SHALL NOT cache abs directly (http://jsperf.com/math-vs-global/2)

	var _RGBtoYUV = function _RGBtoYUV(c) {
	  var r = (c & 0xFF0000) >> 16;
	  var g = (c & 0x00FF00) >> 8;
	  var b = c & 0x0000FF;
	  return (( /*y=*/0.299 * r + 0.587 * g + 0.114 * b | 0) << 16) + (( /*u=*/-0.169 * r - 0.331 * g + 0.5 * b + 128 | 0) << 8) + ( /*v=*/0.5 * r - 0.419 * g - 0.081 * b + 128 | 0);
	};

	var _Diff = function _Diff(w1, w2) {
	  // Mask against RGB_MASK to discard the alpha channel
	  var YUV1 = _RGBtoYUV(w1);
	  var YUV2 = _RGBtoYUV(w2);
	  return _Math.abs((YUV1 & _Ymask) - (YUV2 & _Ymask)) > _trY || _Math.abs((YUV1 & _Umask) - (YUV2 & _Umask)) > _trU || _Math.abs((YUV1 & _Vmask) - (YUV2 & _Vmask)) > _trV;
	};

	/* Interpolate functions */

	var _Interp1 = function _Interp1(pc, c1, c2) {
	  //*pc = (c1*3+c2) >> 2;
	  if (c1 === c2) {
	    _dest[pc] = c1;
	    return;
	  }
	  _dest[pc] = ((c1 & _MASK_2) * 3 + (c2 & _MASK_2) >> 2 & _MASK_2) + ((c1 & _MASK_13) * 3 + (c2 & _MASK_13) >> 2 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp2 = function _Interp2(pc, c1, c2, c3) {
	  //*pc = (c1*2+c2+c3) >> 2;
	  _dest[pc] = (((c1 & _MASK_2) << 1) + (c2 & _MASK_2) + (c3 & _MASK_2) >> 2 & _MASK_2) + (((c1 & _MASK_13) << 1) + (c2 & _MASK_13) + (c3 & _MASK_13) >> 2 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp3 = function _Interp3(pc, c1, c2) {
	  //*pc = (c1*7+c2)/8;
	  if (c1 === c2) {
	    _dest[pc] = c1;
	    return;
	  }
	  _dest[pc] = ((c1 & _MASK_2) * 7 + (c2 & _MASK_2) >> 3 & _MASK_2) + ((c1 & _MASK_13) * 7 + (c2 & _MASK_13) >> 3 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp4 = function _Interp4(pc, c1, c2, c3) {
	  //*pc = (c1*2+(c2+c3)*7)/16;
	  _dest[pc] = (((c1 & _MASK_2) << 1) + (c2 & _MASK_2) * 7 + (c3 & _MASK_2) * 7 >> 4 & _MASK_2) + (((c1 & _MASK_13) << 1) + (c2 & _MASK_13) * 7 + (c3 & _MASK_13) * 7 >> 4 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp5 = function _Interp5(pc, c1, c2) {
	  //*pc = (c1+c2) >> 1;
	  if (c1 === c2) {
	    _dest[pc] = c1;
	    return;
	  }
	  _dest[pc] = ((c1 & _MASK_2) + (c2 & _MASK_2) >> 1 & _MASK_2) + ((c1 & _MASK_13) + (c2 & _MASK_13) >> 1 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp6 = function _Interp6(pc, c1, c2, c3) {
	  //*pc = (c1*5+c2*2+c3)/8;
	  _dest[pc] = ((c1 & _MASK_2) * 5 + ((c2 & _MASK_2) << 1) + (c3 & _MASK_2) >> 3 & _MASK_2) + ((c1 & _MASK_13) * 5 + ((c2 & _MASK_13) << 1) + (c3 & _MASK_13) >> 3 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp7 = function _Interp7(pc, c1, c2, c3) {
	  //*pc = (c1*6+c2+c3)/8;
	  _dest[pc] = ((c1 & _MASK_2) * 6 + (c2 & _MASK_2) + (c3 & _MASK_2) >> 3 & _MASK_2) + ((c1 & _MASK_13) * 6 + (c2 & _MASK_13) + (c3 & _MASK_13) >> 3 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp8 = function _Interp8(pc, c1, c2) {
	  //*pc = (c1*5+c2*3)/8;
	  if (c1 === c2) {
	    _dest[pc] = c1;
	    return;
	  }
	  _dest[pc] = ((c1 & _MASK_2) * 5 + (c2 & _MASK_2) * 3 >> 3 & _MASK_2) + ((c1 & _MASK_13) * 5 + (c2 & _MASK_13) * 3 >> 3 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var _Interp9 = function _Interp9(pc, c1, c2, c3) {
	  //*pc = (c1*2+(c2+c3)*3)/8;
	  _dest[pc] = (((c1 & _MASK_2) << 1) + (c2 & _MASK_2) * 3 + (c3 & _MASK_2) * 3 >> 3 & _MASK_2) + (((c1 & _MASK_13) << 1) + (c2 & _MASK_13) * 3 + (c3 & _MASK_13) * 3 >> 3 & _MASK_13);

	  _dest[pc] |= c1 & 0xFF000000;
	};

	var getVendorAttribute = function getVendorAttribute(el, attr) {
	  var uc = attr.charAt(0).toUpperCase() + attr.substr(1);
	  return el[attr] || el['ms' + uc] || el['moz' + uc] || el['webkit' + uc] || el['o' + uc];
	};

	// This function normalizes getImageData to extract the real, actual
	// pixels from an image. The naive method recently failed on retina
	// devices with a backgingStoreRatio != 1
	var getImagePixels = function getImagePixels(image, x, y, width, height) {
	  var canvas = document.createElement('canvas');
	  var ctx = canvas.getContext('2d');

	  var ratio = getVendorAttribute(ctx, 'backingStorePixelRatio') || 1;
	  ctx.getImageDataHD = getVendorAttribute(ctx, 'getImageDataHD');

	  var realWidth = image.width / ratio,
	      realHeight = image.height / ratio;

	  canvas.width = Math.ceil(realWidth);
	  canvas.height = Math.ceil(realHeight);

	  ctx.drawImage(image, 0, 0, realWidth, realHeight);

	  return ratio === 1 ? ctx.getImageData(x, y, width, height) : ctx.getImageDataHD(x, y, width, height);
	};

	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	// hq 2x

	var hq2x = function hq2x(width, height) {
	  var i,
	      j,
	      k,
	      prevline,
	      nextline,
	      w = [],

	  //dpL = width * 2, optimized
	  dpL = width << 1,
	      dp = 0,
	      sp = 0;

	  // internal to local optimization
	  var Diff = _Diff,
	      Math = _Math,
	      RGBtoYUV = _RGBtoYUV,
	      Interp1 = _Interp1,
	      Interp2 = _Interp2,
	      Interp6 = _Interp6,
	      Interp7 = _Interp7,
	      Interp9 = _Interp9,
	      src = _src,
	      dest = _dest,
	      Ymask = _Ymask,
	      Umask = _Umask,
	      Vmask = _Vmask,
	      trY = _trY,
	      trU = _trU,
	      trV = _trV,
	      YUV1,
	      YUV2;

	  //   +----+----+----+
	  //   |    |    |    |
	  //   | w1 | w2 | w3 |
	  //   +----+----+----+
	  //   |    |    |    |
	  //   | w4 | w5 | w6 |
	  //   +----+----+----+
	  //   |    |    |    |
	  //   | w7 | w8 | w9 |
	  //   +----+----+----+

	  for (j = 0; j < height; j++) {
	    prevline = j > 0 ? -width : 0;
	    nextline = j < height - 1 ? width : 0;

	    for (i = 0; i < width; i++) {
	      w[2] = src[sp + prevline];
	      w[5] = src[sp];
	      w[8] = src[sp + nextline];

	      if (i > 0) {
	        w[1] = src[sp + prevline - 1];
	        w[4] = src[sp - 1];
	        w[7] = src[sp + nextline - 1];
	      } else {
	        w[1] = w[2];
	        w[4] = w[5];
	        w[7] = w[8];
	      }

	      if (i < width - 1) {
	        w[3] = src[sp + prevline + 1];
	        w[6] = src[sp + 1];
	        w[9] = src[sp + nextline + 1];
	      } else {
	        w[3] = w[2];
	        w[6] = w[5];
	        w[9] = w[8];
	      }

	      var pattern = 0;
	      var flag = 1;

	      YUV1 = RGBtoYUV(w[5]);

	      //for (k=1; k<=9; k++) optimized
	      for (k = 1; k < 10; k++) // k<=9
	      {
	        if (k === 5) continue;

	        if (w[k] !== w[5]) {
	          YUV2 = RGBtoYUV(w[k]);
	          if (Math.abs((YUV1 & Ymask) - (YUV2 & Ymask)) > trY || Math.abs((YUV1 & Umask) - (YUV2 & Umask)) > trU || Math.abs((YUV1 & Vmask) - (YUV2 & Vmask)) > trV) pattern |= flag;
	        }
	        flag <<= 1;
	      }

	      switch (pattern) {
	        case 0:
	        case 1:
	        case 4:
	        case 32:
	        case 128:
	        case 5:
	        case 132:
	        case 160:
	        case 33:
	        case 129:
	        case 36:
	        case 133:
	        case 164:
	        case 161:
	        case 37:
	        case 165:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 2:
	        case 34:
	        case 130:
	        case 162:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 16:
	        case 17:
	        case 48:
	        case 49:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 64:
	        case 65:
	        case 68:
	        case 69:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 8:
	        case 12:
	        case 136:
	        case 140:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 3:
	        case 35:
	        case 131:
	        case 163:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 6:
	        case 38:
	        case 134:
	        case 166:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 20:
	        case 21:
	        case 52:
	        case 53:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 144:
	        case 145:
	        case 176:
	        case 177:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 192:
	        case 193:
	        case 196:
	        case 197:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 96:
	        case 97:
	        case 100:
	        case 101:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 40:
	        case 44:
	        case 168:
	        case 172:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 9:
	        case 13:
	        case 137:
	        case 141:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 18:
	        case 50:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 80:
	        case 81:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 72:
	        case 76:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 10:
	        case 138:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 66:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 24:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 7:
	        case 39:
	        case 135:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 148:
	        case 149:
	        case 180:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 224:
	        case 228:
	        case 225:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 41:
	        case 169:
	        case 45:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 22:
	        case 54:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 208:
	        case 209:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 104:
	        case 108:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 11:
	        case 139:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 19:
	        case 51:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp1(dp, w[5], w[4]);
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp6(dp, w[5], w[2], w[4]);
	              Interp9(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 146:
	        case 178:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	              Interp1(dp + dpL + 1, w[5], w[8]);
	            } else {
	              Interp9(dp + 1, w[5], w[2], w[6]);
	              Interp6(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            break;
	          }
	        case 84:
	        case 85:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp6(dp + 1, w[5], w[6], w[2]);
	              Interp9(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            break;
	          }
	        case 112:
	        case 113:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL, w[5], w[4]);
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp6(dp + dpL, w[5], w[8], w[4]);
	              Interp9(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 200:
	        case 204:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	              Interp1(dp + dpL + 1, w[5], w[6]);
	            } else {
	              Interp9(dp + dpL, w[5], w[8], w[4]);
	              Interp6(dp + dpL + 1, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 73:
	        case 77:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp1(dp, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp6(dp, w[5], w[4], w[2]);
	              Interp9(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 42:
	        case 170:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	              Interp1(dp + dpL, w[5], w[8]);
	            } else {
	              Interp9(dp, w[5], w[4], w[2]);
	              Interp6(dp + dpL, w[5], w[4], w[8]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 14:
	        case 142:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	              Interp1(dp + 1, w[5], w[6]);
	            } else {
	              Interp9(dp, w[5], w[4], w[2]);
	              Interp6(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 67:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 70:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 28:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 152:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 194:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 98:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 56:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 25:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 26:
	        case 31:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 82:
	        case 214:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 88:
	        case 248:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 74:
	        case 107:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 27:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[3]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 86:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 216:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp1(dp + dpL, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 106:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 30:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 210:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp1(dp + 1, w[5], w[3]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 120:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 75:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp1(dp + dpL, w[5], w[7]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 29:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 198:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 184:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 99:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 57:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 71:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 156:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 226:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 60:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 195:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 102:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 153:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 58:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 83:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 92:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 202:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 78:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 154:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 114:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 89:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 90:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 55:
	        case 23:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp1(dp, w[5], w[4]);
	              dest[dp + 1] = w[5];
	            } else {
	              Interp6(dp, w[5], w[2], w[4]);
	              Interp9(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 182:
	        case 150:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              Interp1(dp + dpL + 1, w[5], w[8]);
	            } else {
	              Interp9(dp + 1, w[5], w[2], w[6]);
	              Interp6(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            break;
	          }
	        case 213:
	        case 212:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + 1, w[5], w[2]);
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp6(dp + 1, w[5], w[6], w[2]);
	              Interp9(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            break;
	          }
	        case 241:
	        case 240:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp6(dp + dpL, w[5], w[8], w[4]);
	              Interp9(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 236:
	        case 232:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              Interp1(dp + dpL + 1, w[5], w[6]);
	            } else {
	              Interp9(dp + dpL, w[5], w[8], w[4]);
	              Interp6(dp + dpL + 1, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 109:
	        case 105:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp1(dp, w[5], w[2]);
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp6(dp, w[5], w[4], w[2]);
	              Interp9(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 171:
	        case 43:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              Interp1(dp + dpL, w[5], w[8]);
	            } else {
	              Interp9(dp, w[5], w[4], w[2]);
	              Interp6(dp + dpL, w[5], w[4], w[8]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 143:
	        case 15:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              Interp1(dp + 1, w[5], w[6]);
	            } else {
	              Interp9(dp, w[5], w[4], w[2]);
	              Interp6(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 124:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 203:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp1(dp + dpL, w[5], w[7]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 62:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 211:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[3]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 118:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 217:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp1(dp + dpL, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 110:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 155:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[3]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 188:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 185:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 61:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 157:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 103:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 227:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 230:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 199:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 220:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 158:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 234:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 242:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 59:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 121:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 87:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 79:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 122:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 94:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 218:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 91:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 229:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 167:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 173:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 181:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 186:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 115:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 93:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 206:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 205:
	        case 201:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + dpL, w[5], w[7]);
	            } else {
	              Interp7(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 174:
	        case 46:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[4]);
	            } else {
	              Interp7(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 179:
	        case 147:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 1, w[5], w[3]);
	            } else {
	              Interp7(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 117:
	        case 116:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL + 1, w[5], w[9]);
	            } else {
	              Interp7(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 189:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 231:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 126:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 219:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 125:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp1(dp, w[5], w[2]);
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp6(dp, w[5], w[4], w[2]);
	              Interp9(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 221:
	          {
	            Interp1(dp, w[5], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + 1, w[5], w[2]);
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp6(dp + 1, w[5], w[6], w[2]);
	              Interp9(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            Interp1(dp + dpL, w[5], w[7]);
	            break;
	          }
	        case 207:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              Interp1(dp + 1, w[5], w[6]);
	            } else {
	              Interp9(dp, w[5], w[4], w[2]);
	              Interp6(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[7]);
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 238:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              Interp1(dp + dpL + 1, w[5], w[6]);
	            } else {
	              Interp9(dp + dpL, w[5], w[8], w[4]);
	              Interp6(dp + dpL + 1, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 190:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              Interp1(dp + dpL + 1, w[5], w[8]);
	            } else {
	              Interp9(dp + 1, w[5], w[2], w[6]);
	              Interp6(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            Interp1(dp + dpL, w[5], w[8]);
	            break;
	          }
	        case 187:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              Interp1(dp + dpL, w[5], w[8]);
	            } else {
	              Interp9(dp, w[5], w[4], w[2]);
	              Interp6(dp + dpL, w[5], w[4], w[8]);
	            }
	            Interp1(dp + 1, w[5], w[3]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 243:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[3]);
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp6(dp + dpL, w[5], w[8], w[4]);
	              Interp9(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 119:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp1(dp, w[5], w[4]);
	              dest[dp + 1] = w[5];
	            } else {
	              Interp6(dp, w[5], w[2], w[4]);
	              Interp9(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 237:
	        case 233:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[2], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp1(dp + dpL, w[5], w[7]);            }
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 175:
	        case 47:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);            }
	            Interp1(dp + 1, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            break;
	          }
	        case 183:
	        case 151:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp1(dp + 1, w[5], w[3]);            }
	            Interp2(dp + dpL, w[5], w[8], w[4]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 245:
	        case 244:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp1(dp + dpL + 1, w[5], w[9]);            }
	            break;
	          }
	        case 250:
	          {
	            Interp1(dp, w[5], w[4]);
	            Interp1(dp + 1, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 123:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 95:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[7]);
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 222:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 252:
	          {
	            Interp2(dp, w[5], w[1], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp1(dp + dpL + 1, w[5], w[9]);            }
	            break;
	          }
	        case 249:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp2(dp + 1, w[5], w[3], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp1(dp + dpL, w[5], w[7]);            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 235:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp2(dp + 1, w[5], w[3], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp1(dp + dpL, w[5], w[7]);            }
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 111:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);            }
	            Interp1(dp + 1, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp2(dp + dpL + 1, w[5], w[9], w[6]);
	            break;
	          }
	        case 63:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp2(dp + dpL + 1, w[5], w[9], w[8]);
	            break;
	          }
	        case 159:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp1(dp + 1, w[5], w[3]);            }
	            Interp2(dp + dpL, w[5], w[7], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 215:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp1(dp + 1, w[5], w[3]);            }
	            Interp2(dp + dpL, w[5], w[7], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 246:
	          {
	            Interp2(dp, w[5], w[1], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp1(dp + dpL + 1, w[5], w[9]);            }
	            break;
	          }
	        case 254:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp1(dp + dpL + 1, w[5], w[9]);            }
	            break;
	          }
	        case 253:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp1(dp + dpL, w[5], w[7]);            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp1(dp + dpL + 1, w[5], w[9]);            }
	            break;
	          }
	        case 251:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            Interp1(dp + 1, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp1(dp + dpL, w[5], w[7]);            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 239:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);            }
	            Interp1(dp + 1, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp1(dp + dpL, w[5], w[7]);            }
	            Interp1(dp + dpL + 1, w[5], w[6]);
	            break;
	          }
	        case 127:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp2(dp + 1, w[5], w[2], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp + dpL, w[5], w[8], w[4]);
	            }
	            Interp1(dp + dpL + 1, w[5], w[9]);
	            break;
	          }
	        case 191:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp1(dp + 1, w[5], w[3]);            }
	            Interp1(dp + dpL, w[5], w[8]);
	            Interp1(dp + dpL + 1, w[5], w[8]);
	            break;
	          }
	        case 223:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp1(dp + 1, w[5], w[3]);            }
	            Interp1(dp + dpL, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp2(dp + dpL + 1, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 247:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp1(dp + 1, w[5], w[3]);            }
	            Interp1(dp + dpL, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp1(dp + dpL + 1, w[5], w[9]);            }
	            break;
	          }
	        case 255:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	            } else {
	              Interp1(dp + 1, w[5], w[3]);            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp1(dp + dpL, w[5], w[7]);            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp1(dp + dpL + 1, w[5], w[9]);            }
	            break;
	          }
	      }
	      sp++;
	      dp += 2;
	    }
	    dp += dpL;
	  }
	};

	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	// hq 3x


	var hq3x = function hq3x(width, height) {
	  var i,
	      j,
	      k,
	      prevline,
	      nextline,
	      w = [],
	      dpL = width * 3,
	      dp = 0,
	      sp = 0;

	  // internal to local optimization
	  var Diff = _Diff,
	      Math = _Math,
	      RGBtoYUV = _RGBtoYUV,
	      Interp1 = _Interp1,
	      Interp2 = _Interp2,
	      Interp3 = _Interp3,
	      Interp4 = _Interp4,
	      Interp5 = _Interp5,
	      src = _src,
	      dest = _dest,
	      Ymask = _Ymask,
	      Umask = _Umask,
	      Vmask = _Vmask,
	      trY = _trY,
	      trU = _trU,
	      trV = _trV,
	      YUV1,
	      YUV2;

	  //   +----+----+----+
	  //   |	|	|	|
	  //   | w1 | w2 | w3 |
	  //   +----+----+----+
	  //   |	|	|	|
	  //   | w4 | w5 | w6 |
	  //   +----+----+----+
	  //   |	|	|	|
	  //   | w7 | w8 | w9 |
	  //   +----+----+----+

	  for (j = 0; j < height; j++) {
	    prevline = j > 0 ? -width : 0;
	    nextline = j < height - 1 ? width : 0;

	    for (i = 0; i < width; i++) {
	      w[2] = src[sp + prevline];
	      w[5] = src[sp];
	      w[8] = src[sp + nextline];

	      if (i > 0) {
	        w[1] = src[sp + prevline - 1];
	        w[4] = src[sp - 1];
	        w[7] = src[sp + nextline - 1];
	      } else {
	        w[1] = w[2];
	        w[4] = w[5];
	        w[7] = w[8];
	      }

	      if (i < width - 1) {
	        w[3] = src[sp + prevline + 1];
	        w[6] = src[sp + 1];
	        w[9] = src[sp + nextline + 1];
	      } else {
	        w[3] = w[2];
	        w[6] = w[5];
	        w[9] = w[8];
	      }

	      var pattern = 0;
	      var flag = 1;

	      YUV1 = RGBtoYUV(w[5]);

	      //for (k=1; k<=9; k++) optimized
	      for (k = 1; k < 10; k++) // k<=9
	      {
	        if (k === 5) continue;

	        if (w[k] !== w[5]) {
	          YUV2 = RGBtoYUV(w[k]);
	          if (Math.abs((YUV1 & Ymask) - (YUV2 & Ymask)) > trY || Math.abs((YUV1 & Umask) - (YUV2 & Umask)) > trU || Math.abs((YUV1 & Vmask) - (YUV2 & Vmask)) > trV) pattern |= flag;
	        }
	        flag <<= 1;
	      }

	      switch (pattern) {
	        case 0:
	        case 1:
	        case 4:
	        case 32:
	        case 128:
	        case 5:
	        case 132:
	        case 160:
	        case 33:
	        case 129:
	        case 36:
	        case 133:
	        case 164:
	        case 161:
	        case 37:
	        case 165:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 2:
	        case 34:
	        case 130:
	        case 162:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 16:
	        case 17:
	        case 48:
	        case 49:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 64:
	        case 65:
	        case 68:
	        case 69:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 8:
	        case 12:
	        case 136:
	        case 140:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 3:
	        case 35:
	        case 131:
	        case 163:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 6:
	        case 38:
	        case 134:
	        case 166:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 20:
	        case 21:
	        case 52:
	        case 53:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 144:
	        case 145:
	        case 176:
	        case 177:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 192:
	        case 193:
	        case 196:
	        case 197:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 96:
	        case 97:
	        case 100:
	        case 101:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 40:
	        case 44:
	        case 168:
	        case 172:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 9:
	        case 13:
	        case 137:
	        case 141:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 18:
	        case 50:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              Interp1(dp + 2, w[5], w[3]);
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 80:
	        case 81:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 72:
	        case 76:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 10:
	        case 138:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 66:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 24:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 7:
	        case 39:
	        case 135:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 148:
	        case 149:
	        case 180:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 224:
	        case 228:
	        case 225:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 41:
	        case 169:
	        case 45:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 22:
	        case 54:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 208:
	        case 209:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 104:
	        case 108:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 11:
	        case 139:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 19:
	        case 51:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp1(dp, w[5], w[4]);
	              dest[dp + 1] = w[5];
	              Interp1(dp + 2, w[5], w[3]);
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp5(dp + 2, w[2], w[6]);
	              Interp1(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 146:
	        case 178:
	          {
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              Interp1(dp + 2, w[5], w[3]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            } else {
	              Interp1(dp + 1, w[5], w[2]);
	              Interp5(dp + 2, w[2], w[6]);
	              Interp1(dp + dpL + 2, w[6], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            break;
	          }
	        case 84:
	        case 85:
	          {
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + 2, w[5], w[2]);
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	              Interp1(dp + dpL + 2, w[6], w[5]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[6], w[8]);
	            }
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            break;
	          }
	        case 112:
	        case 113:
	          {
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp1(dp + dpL + 2, w[5], w[6]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[8], w[5]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[6], w[8]);
	            }
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            break;
	          }
	        case 200:
	        case 204:
	          {
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            } else {
	              Interp1(dp + dpL, w[5], w[4]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[8], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            break;
	          }
	        case 73:
	        case 77:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp1(dp, w[5], w[2]);
	              dest[dp + dpL] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 42:
	        case 170:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            } else {
	              Interp5(dp, w[4], w[2]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 14:
	        case 142:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	              dest[dp + 1] = w[5];
	              Interp1(dp + 2, w[5], w[6]);
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[4], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp2(dp + 2, w[5], w[2], w[6]);
	              Interp1(dp + dpL, w[5], w[4]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 67:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 70:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 28:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 152:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 194:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 98:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 56:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 25:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 26:
	        case 31:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 82:
	        case 214:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 88:
	        case 248:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 74:
	        case 107:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 27:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 86:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 216:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 106:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 30:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 210:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 120:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 75:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 29:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 198:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 184:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 99:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 57:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 71:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 156:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 226:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 60:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 195:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 102:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 153:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 58:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 83:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 92:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 202:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 78:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 154:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 114:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 89:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 90:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 55:
	        case 23:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp1(dp, w[5], w[4]);
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp5(dp + 2, w[2], w[6]);
	              Interp1(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 182:
	        case 150:
	          {
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            } else {
	              Interp1(dp + 1, w[5], w[2]);
	              Interp5(dp + 2, w[2], w[6]);
	              Interp1(dp + dpL + 2, w[6], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            break;
	          }
	        case 213:
	        case 212:
	          {
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + 2, w[5], w[2]);
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	              Interp1(dp + dpL + 2, w[6], w[5]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[6], w[8]);
	            }
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            break;
	          }
	        case 241:
	        case 240:
	          {
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp1(dp + dpL + 2, w[5], w[6]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[8], w[5]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[6], w[8]);
	            }
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            break;
	          }
	        case 236:
	        case 232:
	          {
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            } else {
	              Interp1(dp + dpL, w[5], w[4]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[8], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            break;
	          }
	        case 109:
	        case 105:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp1(dp, w[5], w[2]);
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 171:
	        case 43:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            } else {
	              Interp5(dp, w[4], w[2]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 143:
	        case 15:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              Interp1(dp + 2, w[5], w[6]);
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[4], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp2(dp + 2, w[5], w[2], w[6]);
	              Interp1(dp + dpL, w[5], w[4]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 124:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 203:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 62:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 211:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 118:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 217:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 110:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 155:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 188:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 185:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 61:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 157:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 103:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 227:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 230:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 199:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 220:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 158:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 234:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 242:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 59:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 121:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 87:
	          {
	            Interp1(dp, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 79:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 122:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 94:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 218:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 91:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 229:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 167:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 173:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 181:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 186:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 115:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 93:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 206:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 205:
	        case 201:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 174:
	        case 46:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp1(dp, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 179:
	        case 147:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 117:
	        case 116:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 189:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 231:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 126:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 219:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 125:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp1(dp, w[5], w[2]);
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 221:
	          {
	            if (Diff(w[6], w[8])) {
	              Interp1(dp + 2, w[5], w[2]);
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	              Interp1(dp + dpL + 2, w[6], w[5]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            break;
	          }
	        case 207:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              Interp1(dp + 2, w[5], w[6]);
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[4], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp2(dp + 2, w[5], w[2], w[6]);
	              Interp1(dp + dpL, w[5], w[4]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 238:
	          {
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            } else {
	              Interp1(dp + dpL, w[5], w[4]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[8], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            break;
	          }
	        case 190:
	          {
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            } else {
	              Interp1(dp + 1, w[5], w[2]);
	              Interp5(dp + 2, w[2], w[6]);
	              Interp1(dp + dpL + 2, w[6], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[1]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            break;
	          }
	        case 187:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            } else {
	              Interp5(dp, w[4], w[2]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 243:
	          {
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp1(dp + dpL + 2, w[5], w[6]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[8], w[5]);
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[6], w[8]);
	            }
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            break;
	          }
	        case 119:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp1(dp, w[5], w[4]);
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp5(dp + 2, w[2], w[6]);
	              Interp1(dp + dpL + 2, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 237:
	        case 233:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp2(dp + 2, w[5], w[2], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 175:
	        case 47:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            break;
	          }
	        case 183:
	        case 151:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 245:
	        case 244:
	          {
	            Interp2(dp, w[5], w[4], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 250:
	          {
	            Interp1(dp, w[5], w[1]);
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 123:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 95:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 222:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 252:
	          {
	            Interp1(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 249:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 235:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 111:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 63:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 159:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 215:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 246:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 254:
	          {
	            Interp1(dp, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp4(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 253:
	          {
	            Interp1(dp, w[5], w[2]);
	            Interp1(dp + 1, w[5], w[2]);
	            Interp1(dp + 2, w[5], w[2]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 251:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp3(dp + dpL, w[5], w[4]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + dpL + 2, w[5], w[6]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 239:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            Interp1(dp + 2, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + dpL + 2, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            break;
	          }
	        case 127:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	              Interp3(dp + 1, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp4(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            } else {
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            break;
	          }
	        case 191:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            break;
	          }
	        case 223:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp4(dp, w[5], w[4], w[2]);
	              Interp3(dp + dpL, w[5], w[4]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 1] = w[5];
	              dest[dp + 2] = w[5];
	              dest[dp + dpL + 2] = w[5];
	            } else {
	              Interp3(dp + 1, w[5], w[2]);
	              Interp2(dp + 2, w[5], w[2], w[6]);
	              Interp3(dp + dpL + 2, w[5], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	              Interp4(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 247:
	          {
	            Interp1(dp, w[5], w[4]);
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[4]);
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	        case 255:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[4], w[2]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	            } else {
	              Interp2(dp + 2, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8], w[4]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            }
	            break;
	          }
	      }
	      sp++;
	      dp += 3;
	    }
	    //dp += (dpL * 2); optimized
	    dp += dpL << 1;
	  }
	};

	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	// hq 4x

	var hq4x = function hq4x(width, height) {
	  var i,
	      j,
	      k,
	      prevline,
	      nextline,
	      w = [],

	  //dpL = width * 4, optimized
	  dpL = width << 2,
	      dp = 0,
	      sp = 0;

	  // internal to local optimization
	  var Diff = _Diff,
	      Math = _Math,
	      RGBtoYUV = _RGBtoYUV,
	      Interp1 = _Interp1,
	      Interp2 = _Interp2,
	      Interp3 = _Interp3,
	      Interp5 = _Interp5,
	      Interp6 = _Interp6,
	      Interp7 = _Interp7,
	      Interp8 = _Interp8,
	      src = _src,
	      dest = _dest,
	      Ymask = _Ymask,
	      Umask = _Umask,
	      Vmask = _Vmask,
	      trY = _trY,
	      trU = _trU,
	      trV = _trV,
	      YUV1,
	      YUV2;

	  //   +----+----+----+
	  //   |    |    |    |
	  //   | w1 | w2 | w3 |
	  //   +----+----+----+
	  //   |    |    |    |
	  //   | w4 | w5 | w6 |
	  //   +----+----+----+
	  //   |    |    |    |
	  //   | w7 | w8 | w9 |
	  //   +----+----+----+

	  for (j = 0; j < height; j++) {
	    prevline = j > 0 ? -width : 0;
	    nextline = j < height - 1 ? width : 0;

	    for (i = 0; i < width; i++) {
	      w[2] = src[sp + prevline];
	      w[5] = src[sp];
	      w[8] = src[sp + nextline];

	      if (i > 0) {
	        w[1] = src[sp + prevline - 1];
	        w[4] = src[sp - 1];
	        w[7] = src[sp + nextline - 1];
	      } else {
	        w[1] = w[2];
	        w[4] = w[5];
	        w[7] = w[8];
	      }

	      if (i < width - 1) {
	        w[3] = src[sp + prevline + 1];
	        w[6] = src[sp + 1];
	        w[9] = src[sp + nextline + 1];
	      } else {
	        w[3] = w[2];
	        w[6] = w[5];
	        w[9] = w[8];
	      }

	      var pattern = 0;
	      var flag = 1;

	      YUV1 = RGBtoYUV(w[5]);

	      //for (k=1; k<=9; k++) optimized
	      for (k = 1; k < 10; k++) // k<=9
	      {
	        if (k === 5) continue;

	        if (w[k] !== w[5]) {
	          YUV2 = RGBtoYUV(w[k]);
	          if (Math.abs((YUV1 & Ymask) - (YUV2 & Ymask)) > trY || Math.abs((YUV1 & Umask) - (YUV2 & Umask)) > trU || Math.abs((YUV1 & Vmask) - (YUV2 & Vmask)) > trV) pattern |= flag;
	        }
	        flag <<= 1;
	      }

	      switch (pattern) {
	        case 0:
	        case 1:
	        case 4:
	        case 32:
	        case 128:
	        case 5:
	        case 132:
	        case 160:
	        case 33:
	        case 129:
	        case 36:
	        case 133:
	        case 164:
	        case 161:
	        case 37:
	        case 165:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 2:
	        case 34:
	        case 130:
	        case 162:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 16:
	        case 17:
	        case 48:
	        case 49:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 64:
	        case 65:
	        case 68:
	        case 69:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 8:
	        case 12:
	        case 136:
	        case 140:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 3:
	        case 35:
	        case 131:
	        case 163:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 6:
	        case 38:
	        case 134:
	        case 166:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 20:
	        case 21:
	        case 52:
	        case 53:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 144:
	        case 145:
	        case 176:
	        case 177:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 192:
	        case 193:
	        case 196:
	        case 197:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 96:
	        case 97:
	        case 100:
	        case 101:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 40:
	        case 44:
	        case 168:
	        case 172:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 9:
	        case 13:
	        case 137:
	        case 141:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 18:
	        case 50:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 80:
	        case 81:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 72:
	        case 76:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 10:
	        case 138:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 66:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 24:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 7:
	        case 39:
	        case 135:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 148:
	        case 149:
	        case 180:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 224:
	        case 228:
	        case 225:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 41:
	        case 169:
	        case 45:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 22:
	        case 54:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 208:
	        case 209:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 104:
	        case 108:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 11:
	        case 139:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 19:
	        case 51:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp8(dp, w[5], w[4]);
	              Interp3(dp + 1, w[5], w[4]);
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp, w[5], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp8(dp + 2, w[2], w[6]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	              Interp2(dp + dpL + 3, w[6], w[5], w[2]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 146:
	        case 178:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            } else {
	              Interp2(dp + 2, w[2], w[5], w[6]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	              Interp8(dp + dpL + 3, w[6], w[2]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp1(dp + dpL * 3 + 3, w[5], w[6]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            break;
	          }
	        case 84:
	        case 85:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp8(dp + 2, w[5], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp8(dp + 3, w[5], w[2]);
	              Interp3(dp + dpL + 3, w[5], w[2]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              Interp1(dp + 3, w[5], w[6]);
	              Interp1(dp + dpL + 3, w[6], w[5]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	              Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[8]);
	              Interp2(dp + dpL * 3 + 2, w[8], w[5], w[6]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 112:
	        case 113:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp8(dp + dpL * 3, w[5], w[4]);
	              Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5], w[8]);
	              Interp1(dp + dpL * 3, w[5], w[8]);
	              Interp1(dp + dpL * 3 + 1, w[8], w[5]);
	              Interp8(dp + dpL * 3 + 2, w[8], w[6]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            break;
	          }
	        case 200:
	        case 204:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	              Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5], w[8]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp8(dp + dpL * 3 + 1, w[8], w[4]);
	              Interp1(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp1(dp + dpL * 3 + 3, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            break;
	          }
	        case 73:
	        case 77:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp8(dp, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[2]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp, w[5], w[4]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[8]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp2(dp + dpL * 3 + 1, w[8], w[5], w[4]);
	            }
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 42:
	        case 170:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	              Interp8(dp + dpL * 3, w[5], w[8]);
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp2(dp + 1, w[2], w[5], w[4]);
	              Interp8(dp + dpL, w[4], w[2]);
	              Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp1(dp + dpL * 3, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 14:
	        case 142:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp3(dp + 2, w[5], w[6]);
	              Interp8(dp + 3, w[5], w[6]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp8(dp + 1, w[2], w[4]);
	              Interp1(dp + 2, w[2], w[5]);
	              Interp1(dp + 3, w[5], w[2]);
	              Interp2(dp + dpL, w[4], w[5], w[2]);
	              Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            }
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 67:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 70:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 28:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 152:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 194:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 98:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 56:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 25:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 26:
	        case 31:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 82:
	        case 214:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 88:
	        case 248:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            break;
	          }
	        case 74:
	        case 107:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 27:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 86:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 216:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 106:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 30:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 210:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 120:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 75:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 29:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 198:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 184:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 99:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 57:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 71:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 156:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 226:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 60:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 195:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 102:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 153:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 58:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 83:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 92:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 202:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 78:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 154:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 114:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            break;
	          }
	        case 89:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 90:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 55:
	        case 23:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp8(dp, w[5], w[4]);
	              Interp3(dp + 1, w[5], w[4]);
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp1(dp, w[5], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp8(dp + 2, w[2], w[6]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	              Interp2(dp + dpL + 3, w[6], w[5], w[2]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 182:
	        case 150:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + dpL + 3] = w[5];
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            } else {
	              Interp2(dp + 2, w[2], w[5], w[6]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	              Interp8(dp + dpL + 3, w[6], w[2]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp1(dp + dpL * 3 + 3, w[5], w[6]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            break;
	          }
	        case 213:
	        case 212:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp8(dp + 2, w[5], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp8(dp + 3, w[5], w[2]);
	              Interp3(dp + dpL + 3, w[5], w[2]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp1(dp + 3, w[5], w[6]);
	              Interp1(dp + dpL + 3, w[6], w[5]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	              Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[8]);
	              Interp2(dp + dpL * 3 + 2, w[8], w[5], w[6]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 241:
	        case 240:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              Interp8(dp + dpL * 3, w[5], w[4]);
	              Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5], w[8]);
	              Interp1(dp + dpL * 3, w[5], w[8]);
	              Interp1(dp + dpL * 3 + 1, w[8], w[5]);
	              Interp8(dp + dpL * 3 + 2, w[8], w[6]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            break;
	          }
	        case 236:
	        case 232:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	              Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5], w[8]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp8(dp + dpL * 3 + 1, w[8], w[4]);
	              Interp1(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp1(dp + dpL * 3 + 3, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            break;
	          }
	        case 109:
	        case 105:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp8(dp, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[2]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[8]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp2(dp + dpL * 3 + 1, w[8], w[5], w[4]);
	            }
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 171:
	        case 43:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	              dest[dp + dpL + 1] = w[5];
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	              Interp8(dp + dpL * 3, w[5], w[8]);
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp2(dp + 1, w[2], w[5], w[4]);
	              Interp8(dp + dpL, w[4], w[2]);
	              Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp1(dp + dpL * 3, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 143:
	        case 15:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              Interp3(dp + 2, w[5], w[6]);
	              Interp8(dp + 3, w[5], w[6]);
	              dest[dp + dpL] = w[5];
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp8(dp + 1, w[2], w[4]);
	              Interp1(dp + 2, w[2], w[5]);
	              Interp1(dp + 3, w[5], w[2]);
	              Interp2(dp + dpL, w[4], w[5], w[2]);
	              Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            }
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 124:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 203:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 62:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 211:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 118:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 217:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 110:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 155:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 188:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 185:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 61:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 157:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 103:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 227:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 230:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 199:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 220:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            break;
	          }
	        case 158:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 234:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 242:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            break;
	          }
	        case 59:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 121:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 87:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            dest[dp + dpL + 2] = w[5];
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 79:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 122:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 94:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 218:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            break;
	          }
	        case 91:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 229:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 167:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 173:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 181:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 186:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 115:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            break;
	          }
	        case 93:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 206:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 205:
	        case 201:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            if (Diff(w[8], w[4])) {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	              Interp8(dp + dpL * 3, w[5], w[7]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            } else {
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	              Interp1(dp + dpL * 3 + 1, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 174:
	        case 46:
	          {
	            if (Diff(w[4], w[2])) {
	              Interp8(dp, w[5], w[1]);
	              Interp1(dp + 1, w[5], w[1]);
	              Interp1(dp + dpL, w[5], w[1]);
	              Interp3(dp + dpL + 1, w[5], w[1]);
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	              Interp1(dp + 1, w[5], w[2]);
	              Interp1(dp + dpL, w[5], w[4]);
	              dest[dp + dpL + 1] = w[5];
	            }
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 179:
	        case 147:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            if (Diff(w[2], w[6])) {
	              Interp1(dp + 2, w[5], w[3]);
	              Interp8(dp + 3, w[5], w[3]);
	              Interp3(dp + dpL + 2, w[5], w[3]);
	              Interp1(dp + dpL + 3, w[5], w[3]);
	            } else {
	              Interp1(dp + 2, w[5], w[2]);
	              Interp2(dp + 3, w[5], w[2], w[6]);
	              dest[dp + dpL + 2] = w[5];
	              Interp1(dp + dpL + 3, w[5], w[6]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 117:
	        case 116:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            } else {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	              Interp1(dp + dpL * 3 + 2, w[5], w[8]);
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            break;
	          }
	        case 189:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 231:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 126:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 219:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 125:
	          {
	            if (Diff(w[8], w[4])) {
	              Interp8(dp, w[5], w[2]);
	              Interp3(dp + dpL, w[5], w[2]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp1(dp, w[5], w[4]);
	              Interp1(dp + dpL, w[4], w[5]);
	              Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[8]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp2(dp + dpL * 3 + 1, w[8], w[5], w[4]);
	            }
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 221:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            if (Diff(w[6], w[8])) {
	              Interp8(dp + 3, w[5], w[2]);
	              Interp3(dp + dpL + 3, w[5], w[2]);
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp1(dp + 3, w[5], w[6]);
	              Interp1(dp + dpL + 3, w[6], w[5]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	              Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[8]);
	              Interp2(dp + dpL * 3 + 2, w[8], w[5], w[6]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 207:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              Interp3(dp + 2, w[5], w[6]);
	              Interp8(dp + 3, w[5], w[6]);
	              dest[dp + dpL] = w[5];
	              dest[dp + dpL + 1] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp8(dp + 1, w[2], w[4]);
	              Interp1(dp + 2, w[2], w[5]);
	              Interp1(dp + 3, w[5], w[2]);
	              Interp2(dp + dpL, w[4], w[5], w[2]);
	              Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            }
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 238:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	              Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            } else {
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5], w[8]);
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp8(dp + dpL * 3 + 1, w[8], w[4]);
	              Interp1(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp1(dp + dpL * 3 + 3, w[5], w[8]);
	            }
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            break;
	          }
	        case 190:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + dpL + 3] = w[5];
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	              Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            } else {
	              Interp2(dp + 2, w[2], w[5], w[6]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	              Interp8(dp + dpL + 3, w[6], w[2]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp1(dp + dpL * 3 + 3, w[5], w[6]);
	            }
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            break;
	          }
	        case 187:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	              dest[dp + dpL + 1] = w[5];
	              Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	              Interp8(dp + dpL * 3, w[5], w[8]);
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp2(dp + 1, w[2], w[5], w[4]);
	              Interp8(dp + dpL, w[4], w[2]);
	              Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	              Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp1(dp + dpL * 3, w[5], w[4]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 243:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              Interp8(dp + dpL * 3, w[5], w[4]);
	              Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	              Interp2(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5], w[8]);
	              Interp1(dp + dpL * 3, w[5], w[8]);
	              Interp1(dp + dpL * 3 + 1, w[8], w[5]);
	              Interp8(dp + dpL * 3 + 2, w[8], w[6]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            break;
	          }
	        case 119:
	          {
	            if (Diff(w[2], w[6])) {
	              Interp8(dp, w[5], w[4]);
	              Interp3(dp + 1, w[5], w[4]);
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 2] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp1(dp, w[5], w[2]);
	              Interp1(dp + 1, w[2], w[5]);
	              Interp8(dp + 2, w[2], w[6]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	              Interp2(dp + dpL + 3, w[6], w[5], w[2]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 237:
	        case 233:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[6]);
	            Interp2(dp + 3, w[5], w[2], w[6]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp7(dp + dpL + 2, w[5], w[6], w[2]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[2]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL * 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            }
	            dest[dp + dpL * 3 + 1] = w[5];
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 175:
	        case 47:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6], w[8]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[6]);
	            Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            break;
	          }
	        case 183:
	        case 151:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            dest[dp + 2] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 3] = w[5];
	            } else {
	              Interp2(dp + 3, w[5], w[2], w[6]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            dest[dp + dpL + 2] = w[5];
	            dest[dp + dpL + 3] = w[5];
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[8]);
	            Interp7(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[4]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 245:
	        case 244:
	          {
	            Interp2(dp, w[5], w[2], w[4]);
	            Interp6(dp + 1, w[5], w[2], w[4]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp6(dp + dpL, w[5], w[4], w[2]);
	            Interp7(dp + dpL + 1, w[5], w[4], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            dest[dp + dpL * 3 + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 250:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            break;
	          }
	        case 123:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 95:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 222:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 252:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp6(dp + 1, w[5], w[2], w[1]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	            dest[dp + dpL * 3 + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 249:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp6(dp + 2, w[5], w[2], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL * 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            }
	            dest[dp + dpL * 3 + 1] = w[5];
	            break;
	          }
	        case 235:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp6(dp + dpL + 3, w[5], w[6], w[3]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL * 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            }
	            dest[dp + dpL * 3 + 1] = w[5];
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 111:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 63:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp6(dp + dpL * 3 + 2, w[5], w[8], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 159:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            dest[dp + 2] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 3] = w[5];
	            } else {
	              Interp2(dp + 3, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            dest[dp + dpL + 3] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp6(dp + dpL * 3 + 1, w[5], w[8], w[7]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 215:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            dest[dp + 2] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 3] = w[5];
	            } else {
	              Interp2(dp + 3, w[5], w[2], w[6]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            dest[dp + dpL + 2] = w[5];
	            dest[dp + dpL + 3] = w[5];
	            Interp6(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 246:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp6(dp + dpL, w[5], w[4], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            dest[dp + dpL * 3 + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 254:
	          {
	            Interp8(dp, w[5], w[1]);
	            Interp1(dp + 1, w[5], w[1]);
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            Interp1(dp + dpL, w[5], w[1]);
	            Interp3(dp + dpL + 1, w[5], w[1]);
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	            dest[dp + dpL * 3 + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 253:
	          {
	            Interp8(dp, w[5], w[2]);
	            Interp8(dp + 1, w[5], w[2]);
	            Interp8(dp + 2, w[5], w[2]);
	            Interp8(dp + 3, w[5], w[2]);
	            Interp3(dp + dpL, w[5], w[2]);
	            Interp3(dp + dpL + 1, w[5], w[2]);
	            Interp3(dp + dpL + 2, w[5], w[2]);
	            Interp3(dp + dpL + 3, w[5], w[2]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL * 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            }
	            dest[dp + dpL * 3 + 1] = w[5];
	            dest[dp + dpL * 3 + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 251:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            Interp1(dp + 2, w[5], w[3]);
	            Interp8(dp + 3, w[5], w[3]);
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[3]);
	            Interp1(dp + dpL + 3, w[5], w[3]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL * 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            }
	            dest[dp + dpL * 3 + 1] = w[5];
	            break;
	          }
	        case 239:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            Interp3(dp + 2, w[5], w[6]);
	            Interp8(dp + 3, w[5], w[6]);
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            Interp3(dp + dpL + 2, w[5], w[6]);
	            Interp8(dp + dpL + 3, w[5], w[6]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[6]);
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[6]);
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL * 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            }
	            dest[dp + dpL * 3 + 1] = w[5];
	            Interp3(dp + dpL * 3 + 2, w[5], w[6]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[6]);
	            break;
	          }
	        case 127:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 2] = w[5];
	              dest[dp + 3] = w[5];
	              dest[dp + dpL + 3] = w[5];
	            } else {
	              Interp5(dp + 2, w[2], w[5]);
	              Interp5(dp + 3, w[2], w[6]);
	              Interp5(dp + dpL + 3, w[6], w[5]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	              dest[dp + dpL * 3] = w[5];
	              dest[dp + dpL * 3 + 1] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/), w[4], w[5]);
	              Interp5(dp + dpL * 3, w[8], w[4]);
	              Interp5(dp + dpL * 3 + 1, w[8], w[5]);
	            }
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[9]);
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[9]);
	            Interp1(dp + dpL * 3 + 2, w[5], w[9]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[9]);
	            break;
	          }
	        case 191:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            dest[dp + 2] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 3] = w[5];
	            } else {
	              Interp2(dp + 3, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            dest[dp + dpL + 3] = w[5];
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 2, w[5], w[8]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[5], w[8]);
	            Interp8(dp + dpL * 3, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 1, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 2, w[5], w[8]);
	            Interp8(dp + dpL * 3 + 3, w[5], w[8]);
	            break;
	          }
	        case 223:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	              dest[dp + 1] = w[5];
	              dest[dp + dpL] = w[5];
	            } else {
	              Interp5(dp, w[2], w[4]);
	              Interp5(dp + 1, w[2], w[5]);
	              Interp5(dp + dpL, w[4], w[5]);
	            }
	            dest[dp + 2] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 3] = w[5];
	            } else {
	              Interp2(dp + 3, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            dest[dp + dpL + 3] = w[5];
	            Interp1(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[7]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[7]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	              dest[dp + dpL * 3 + 2] = w[5];
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp5(dp + (dpL << 1 /*==dpL * 2*/) + 3, w[6], w[5]);
	              Interp5(dp + dpL * 3 + 2, w[8], w[5]);
	              Interp5(dp + dpL * 3 + 3, w[8], w[6]);
	            }
	            Interp8(dp + dpL * 3, w[5], w[7]);
	            Interp1(dp + dpL * 3 + 1, w[5], w[7]);
	            break;
	          }
	        case 247:
	          {
	            Interp8(dp, w[5], w[4]);
	            Interp3(dp + 1, w[5], w[4]);
	            dest[dp + 2] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 3] = w[5];
	            } else {
	              Interp2(dp + 3, w[5], w[2], w[6]);
	            }
	            Interp8(dp + dpL, w[5], w[4]);
	            Interp3(dp + dpL + 1, w[5], w[4]);
	            dest[dp + dpL + 2] = w[5];
	            dest[dp + dpL + 3] = w[5];
	            Interp8(dp + (dpL << 1 /*==dpL * 2*/), w[5], w[4]);
	            Interp3(dp + (dpL << 1 /*==dpL * 2*/) + 1, w[5], w[4]);
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	            Interp8(dp + dpL * 3, w[5], w[4]);
	            Interp3(dp + dpL * 3 + 1, w[5], w[4]);
	            dest[dp + dpL * 3 + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	        case 255:
	          {
	            if (Diff(w[4], w[2])) {
	              dest[dp] = w[5];
	            } else {
	              Interp2(dp, w[5], w[2], w[4]);
	            }
	            dest[dp + 1] = w[5];
	            dest[dp + 2] = w[5];
	            if (Diff(w[2], w[6])) {
	              dest[dp + 3] = w[5];
	            } else {
	              Interp2(dp + 3, w[5], w[2], w[6]);
	            }
	            dest[dp + dpL] = w[5];
	            dest[dp + dpL + 1] = w[5];
	            dest[dp + dpL + 2] = w[5];
	            dest[dp + dpL + 3] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/)] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 1] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 2] = w[5];
	            dest[dp + (dpL << 1 /*==dpL * 2*/) + 3] = w[5];
	            if (Diff(w[8], w[4])) {
	              dest[dp + dpL * 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3, w[5], w[8], w[4]);
	            }
	            dest[dp + dpL * 3 + 1] = w[5];
	            dest[dp + dpL * 3 + 2] = w[5];
	            if (Diff(w[6], w[8])) {
	              dest[dp + dpL * 3 + 3] = w[5];
	            } else {
	              Interp2(dp + dpL * 3 + 3, w[5], w[8], w[6]);
	            }
	            break;
	          }
	      }
	      sp++;
	      dp += 4;
	    }
	    dp += dpL * 3;
	  }
	};
	module.exports = exports['default'];

	/***/ })
	/******/ ]);
	});

	}(hqx$1));

	var hqx = /*@__PURE__*/getDefaultExportFromCjs(hqx$1.exports);

	var imagetracer_v1_2_6 = {exports: {}};

	/*
		imagetracer.js version 1.2.6
		Simple raster image tracer and vectorizer written in JavaScript.
		andras@jankovics.net
	*/

	(function (module) {
	/*

	The Unlicense / PUBLIC DOMAIN

	This is free and unencumbered software released into the public domain.

	Anyone is free to copy, modify, publish, use, compile, sell, or
	distribute this software, either in source code form or as a compiled
	binary, for any purpose, commercial or non-commercial, and by any
	means.

	In jurisdictions that recognize copyright laws, the author or authors
	of this software dedicate any and all copyright interest in the
	software to the public domain. We make this dedication for the benefit
	of the public at large and to the detriment of our heirs and
	successors. We intend this dedication to be an overt act of
	relinquishment in perpetuity of all present and future rights to this
	software under copyright law.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
	ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.

	For more information, please refer to http://unlicense.org/

	*/

	(function(){
	function ImageTracer(){
		var _this = this;

		this.versionnumber = '1.2.6',
		
		////////////////////////////////////////////////////////////
		//
		//  API
		//
		////////////////////////////////////////////////////////////
		
		// Loading an image from a URL, tracing when loaded,
		// then executing callback with the scaled svg string as argument
		this.imageToSVG = function( url, callback, options ){
			options = _this.checkoptions(options);
			// loading image, tracing and callback
			_this.loadImage(
				url,
				function(canvas){
					callback(
						_this.imagedataToSVG( _this.getImgdata(canvas), options )
					);
				},
				options
			);
		},// End of imageToSVG()
		
		// Tracing imagedata, then returning the scaled svg string
		this.imagedataToSVG = function( imgd, options ){
			options = _this.checkoptions(options);
			// tracing imagedata
			var td = _this.imagedataToTracedata( imgd, options );
			// returning SVG string
			return _this.getsvgstring(td, options);
		},// End of imagedataToSVG()
		
		// Loading an image from a URL, tracing when loaded,
		// then executing callback with tracedata as argument
		this.imageToTracedata = function( url, callback, options ){
			options = _this.checkoptions(options);
			// loading image, tracing and callback
			_this.loadImage(
					url,
					function(canvas){
						callback(
							_this.imagedataToTracedata( _this.getImgdata(canvas), options )
						);
					},
					options
			);
		},// End of imageToTracedata()
		
		// Tracing imagedata, then returning tracedata (layers with paths, palette, image size)
		this.imagedataToTracedata = function( imgd, options ){
			options = _this.checkoptions(options);
			
			// 1. Color quantization
			var ii = _this.colorquantization( imgd, options );
			
			if(options.layering === 0){// Sequential layering
				
				// create tracedata object
				var tracedata = {
					layers : [],
					palette : ii.palette,
					width : ii.array[0].length-2,
					height : ii.array.length-2
				};
				
				// Loop to trace each color layer
				for(var colornum=0; colornum<ii.palette.length; colornum++){
					
					// layeringstep -> pathscan -> internodes -> batchtracepaths
					var tracedlayer =
						_this.batchtracepaths(
								
							_this.internodes(
									
								_this.pathscan(
									_this.layeringstep( ii, colornum ),
									options.pathomit
								),
								
								options
								
							),
							
							options.ltres,
							options.qtres
							
						);
					
					// adding traced layer
					tracedata.layers.push(tracedlayer);
					
				}// End of color loop
				
			}else {// Parallel layering
				// 2. Layer separation and edge detection
				var ls = _this.layering( ii );
				
				// Optional edge node visualization
				if(options.layercontainerid){ _this.drawLayers( ls, _this.specpalette, options.scale, options.layercontainerid ); }
				
				// 3. Batch pathscan
				var bps = _this.batchpathscan( ls, options.pathomit );
				
				// 4. Batch interpollation
				var bis = _this.batchinternodes( bps, options );
				
				// 5. Batch tracing and creating tracedata object
				var tracedata = {
					layers : _this.batchtracelayers( bis, options.ltres, options.qtres ),
					palette : ii.palette,
					width : imgd.width,
					height : imgd.height
				};
				
			}// End of parallel layering
			
			// return tracedata
			return tracedata;
			
		},// End of imagedataToTracedata()
		
		this.optionpresets = {
			'default': {
				
				// Tracing
				corsenabled : false,
				ltres : 1,
				qtres : 1,
				pathomit : 8,
				rightangleenhance : true,
				
				// Color quantization
				colorsampling : 2,
				numberofcolors : 16,
				mincolorratio : 0,
				colorquantcycles : 3,
				
				// Layering method
				layering : 0,
				
				// SVG rendering
				strokewidth : 1,
				linefilter : false,
				scale : 1,
				roundcoords : 1,
				viewbox : false,
				desc : false,
				lcpr : 0,
				qcpr : 0,
				
				// Blur
				blurradius : 0,
				blurdelta : 20
				
			},
			'posterized1': { colorsampling:0, numberofcolors:2 },
			'posterized2': { numberofcolors:4, blurradius:5 },
			'curvy': { ltres:0.01, linefilter:true, rightangleenhance:false },
			'sharp': { qtres:0.01, linefilter:false },
			'detailed': { pathomit:0, roundcoords:2, ltres:0.5, qtres:0.5, numberofcolors:64 },
			'smoothed': { blurradius:5, blurdelta: 64 },
			'grayscale': { colorsampling:0, colorquantcycles:1, numberofcolors:7 },
			'fixedpalette': { colorsampling:0, colorquantcycles:1, numberofcolors:27 },
			'randomsampling1': { colorsampling:1, numberofcolors:8 },
			'randomsampling2': { colorsampling:1, numberofcolors:64 },
			'artistic1': { colorsampling:0, colorquantcycles:1, pathomit:0, blurradius:5, blurdelta: 64, ltres:0.01, linefilter:true, numberofcolors:16, strokewidth:2 },
			'artistic2': { qtres:0.01, colorsampling:0, colorquantcycles:1, numberofcolors:4, strokewidth:0 },
			'artistic3': { qtres:10, ltres:10, numberofcolors:8 },
			'artistic4': { qtres:10, ltres:10, numberofcolors:64, blurradius:5, blurdelta: 256, strokewidth:2 },
			'posterized3': { ltres: 1, qtres: 1, pathomit: 20, rightangleenhance: true, colorsampling: 0, numberofcolors: 3,
				mincolorratio: 0, colorquantcycles: 3, blurradius: 3, blurdelta: 20, strokewidth: 0, linefilter: false,
				roundcoords: 1, pal: [ { r: 0, g: 0, b: 100, a: 255 }, { r: 255, g: 255, b: 255, a: 255 } ] }
		},// End of optionpresets
		
		// creating options object, setting defaults for missing values
		this.checkoptions = function(options){
			options = options || {};
			// Option preset
			if(typeof options === 'string'){
				options = options.toLowerCase();
				if( _this.optionpresets[options] ){ options = _this.optionpresets[options]; }else { options = {}; }
			}
			// Defaults
			var ok = Object.keys(_this.optionpresets['default']);
			for(var k=0; k<ok.length; k++){
				if(!options.hasOwnProperty(ok[k])){ options[ok[k]] = _this.optionpresets['default'][ok[k]]; }
			}
			// options.pal is not defined here, the custom palette should be added externally: options.pal = [ { 'r':0, 'g':0, 'b':0, 'a':255 }, {...}, ... ];
			// options.layercontainerid is not defined here, can be added externally: options.layercontainerid = 'mydiv'; ... <div id="mydiv"></div>
			return options;
		},// End of checkoptions()
		
		////////////////////////////////////////////////////////////
		//
		//  Vectorizing functions
		//
		////////////////////////////////////////////////////////////
		
		// 1. Color quantization
		// Using a form of k-means clustering repeatead options.colorquantcycles times. http://en.wikipedia.org/wiki/Color_quantization
		this.colorquantization = function( imgd, options ){
			var arr = [], idx=0, cd,cdl,ci, paletteacc = [], pixelnum = imgd.width * imgd.height, i, j, k, cnt, palette;
			
			// imgd.data must be RGBA, not just RGB
			if( imgd.data.length < pixelnum * 4 ){
				var newimgddata = new Uint8ClampedArray(pixelnum * 4);
				for(var pxcnt = 0; pxcnt < pixelnum ; pxcnt++){
					newimgddata[pxcnt*4  ] = imgd.data[pxcnt*3  ];
					newimgddata[pxcnt*4+1] = imgd.data[pxcnt*3+1];
					newimgddata[pxcnt*4+2] = imgd.data[pxcnt*3+2];
					newimgddata[pxcnt*4+3] = 255;
				}
				imgd.data = newimgddata;
			}// End of RGBA imgd.data check
			
			// Filling arr (color index array) with -1
			for( j=0; j<imgd.height+2; j++ ){ arr[j]=[]; for(i=0; i<imgd.width+2 ; i++){ arr[j][i] = -1; } }
			
			// Use custom palette if pal is defined or sample / generate custom length palette
			if(options.pal){
				palette = options.pal;
			}else if(options.colorsampling === 0){
				palette = _this.generatepalette(options.numberofcolors);
			}else if(options.colorsampling === 1){
				palette = _this.samplepalette( options.numberofcolors, imgd );
			}else {
				palette = _this.samplepalette2( options.numberofcolors, imgd );
			}
			
			// Selective Gaussian blur preprocessing
			if( options.blurradius > 0 ){ imgd = _this.blur( imgd, options.blurradius, options.blurdelta ); }
			
			// Repeat clustering step options.colorquantcycles times
			for( cnt=0; cnt < options.colorquantcycles; cnt++ ){
				
				// Average colors from the second iteration
				if(cnt>0){
					// averaging paletteacc for palette
					for( k=0; k < palette.length; k++ ){
						
						// averaging
						if( paletteacc[k].n > 0 ){
							palette[k] = {  r: Math.floor( paletteacc[k].r / paletteacc[k].n ),
											g: Math.floor( paletteacc[k].g / paletteacc[k].n ),
											b: Math.floor( paletteacc[k].b / paletteacc[k].n ),
											a:  Math.floor( paletteacc[k].a / paletteacc[k].n ) };
						}
						
						// Randomizing a color, if there are too few pixels and there will be a new cycle
						if( ( paletteacc[k].n/pixelnum < options.mincolorratio ) && ( cnt < options.colorquantcycles-1 ) ){
							palette[k] = {  r: Math.floor(Math.random()*255),
											g: Math.floor(Math.random()*255),
											b: Math.floor(Math.random()*255),
											a: Math.floor(Math.random()*255) };
						}
						
					}// End of palette loop
				}// End of Average colors from the second iteration
				
				// Reseting palette accumulator for averaging
				for( i=0; i < palette.length; i++ ){ paletteacc[i] = { r:0, g:0, b:0, a:0, n:0 }; }
				
				// loop through all pixels
				for( j=0; j < imgd.height; j++ ){
					for( i=0; i < imgd.width; i++ ){
						
						// pixel index
						idx = (j*imgd.width+i)*4;
						
						// find closest color from palette by measuring (rectilinear) color distance between this pixel and all palette colors
						ci=0; cdl = 1024; // 4 * 256 is the maximum RGBA distance
						for( k=0; k<palette.length; k++ ){
							
							// In my experience, https://en.wikipedia.org/wiki/Rectilinear_distance works better than https://en.wikipedia.org/wiki/Euclidean_distance
							cd = Math.abs(palette[k].r-imgd.data[idx]) + Math.abs(palette[k].g-imgd.data[idx+1]) + Math.abs(palette[k].b-imgd.data[idx+2]) + Math.abs(palette[k].a-imgd.data[idx+3]);
							
							// Remember this color if this is the closest yet
							if(cd<cdl){ cdl = cd; ci = k; }
							
						}// End of palette loop
						
						// add to palettacc
						paletteacc[ci].r += imgd.data[idx  ];
						paletteacc[ci].g += imgd.data[idx+1];
						paletteacc[ci].b += imgd.data[idx+2];
						paletteacc[ci].a += imgd.data[idx+3];
						paletteacc[ci].n++;
						
						// update the indexed color array
						arr[j+1][i+1] = ci;
						
					}// End of i loop
				}// End of j loop
				
			}// End of Repeat clustering step options.colorquantcycles times
			
			return { array:arr, palette:palette };
			
		},// End of colorquantization()
		
		// Sampling a palette from imagedata
		this.samplepalette = function( numberofcolors, imgd ){
			var idx, palette=[];
			for(var i=0; i<numberofcolors; i++){
				idx = Math.floor( Math.random() * imgd.data.length / 4 ) * 4;
				palette.push({ r:imgd.data[idx  ], g:imgd.data[idx+1], b:imgd.data[idx+2], a:imgd.data[idx+3] });
			}
			return palette;
		},// End of samplepalette()
		
		// Deterministic sampling a palette from imagedata: rectangular grid
		this.samplepalette2 = function( numberofcolors, imgd ){
			var idx, palette=[], ni = Math.ceil(Math.sqrt(numberofcolors)), nj = Math.ceil(numberofcolors/ni),
				vx = imgd.width / (ni+1), vy = imgd.height / (nj+1);
			for(var j=0; j<nj; j++){
				for(var i=0; i<ni; i++){
					if(palette.length === numberofcolors){
						break;
					}else {
						idx = Math.floor( ((j+1)*vy) * imgd.width + ((i+1)*vx) ) * 4;
						palette.push( { r:imgd.data[idx], g:imgd.data[idx+1], b:imgd.data[idx+2], a:imgd.data[idx+3] } );
					}
				}
			}
			return palette;
		},// End of samplepalette2()
		
		// Generating a palette with numberofcolors
		this.generatepalette = function(numberofcolors){
			var palette = [], rcnt, gcnt, bcnt;
			if(numberofcolors<8){
				
				// Grayscale
				var graystep = Math.floor(255/(numberofcolors-1));
				for(var i=0; i<numberofcolors; i++){ palette.push({ r:i*graystep, g:i*graystep, b:i*graystep, a:255 }); }
				
			}else {
				
				// RGB color cube
				var colorqnum = Math.floor(Math.pow(numberofcolors, 1/3)), // Number of points on each edge on the RGB color cube
					colorstep = Math.floor(255/(colorqnum-1)), // distance between points
					rndnum = numberofcolors - colorqnum*colorqnum*colorqnum; // number of random colors
				
				for(rcnt=0; rcnt<colorqnum; rcnt++){
					for(gcnt=0; gcnt<colorqnum; gcnt++){
						for(bcnt=0; bcnt<colorqnum; bcnt++){
							palette.push( { r:rcnt*colorstep, g:gcnt*colorstep, b:bcnt*colorstep, a:255 } );
						}// End of blue loop
					}// End of green loop
				}// End of red loop
				
				// Rest is random
				for(rcnt=0; rcnt<rndnum; rcnt++){ palette.push({ r:Math.floor(Math.random()*255), g:Math.floor(Math.random()*255), b:Math.floor(Math.random()*255), a:Math.floor(Math.random()*255) }); }

			}// End of numberofcolors check
			
			return palette;
		},// End of generatepalette()
			
		// 2. Layer separation and edge detection
		// Edge node types ( ▓: this layer or 1; ░: not this layer or 0 )
		// 12  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓
		// 48  ░░  ░░  ░░  ░░  ░▓  ░▓  ░▓  ░▓  ▓░  ▓░  ▓░  ▓░  ▓▓  ▓▓  ▓▓  ▓▓
		//     0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
		this.layering = function(ii){
			// Creating layers for each indexed color in arr
			var layers = [], val=0, ah = ii.array.length, aw = ii.array[0].length, n1,n2,n3,n4,n5,n6,n7,n8, i, j, k;
			
			// Create layers
			for(k=0; k<ii.palette.length; k++){
				layers[k] = [];
				for(j=0; j<ah; j++){
					layers[k][j] = [];
					for(i=0; i<aw; i++){
						layers[k][j][i]=0;
					}
				}
			}
			
			// Looping through all pixels and calculating edge node type
			for(j=1; j<ah-1; j++){
				for(i=1; i<aw-1; i++){
					
					// This pixel's indexed color
					val = ii.array[j][i];
					
					// Are neighbor pixel colors the same?
					n1 = ii.array[j-1][i-1]===val ? 1 : 0;
					n2 = ii.array[j-1][i  ]===val ? 1 : 0;
					n3 = ii.array[j-1][i+1]===val ? 1 : 0;
					n4 = ii.array[j  ][i-1]===val ? 1 : 0;
					n5 = ii.array[j  ][i+1]===val ? 1 : 0;
					n6 = ii.array[j+1][i-1]===val ? 1 : 0;
					n7 = ii.array[j+1][i  ]===val ? 1 : 0;
					n8 = ii.array[j+1][i+1]===val ? 1 : 0;
					
					// this pixel's type and looking back on previous pixels
					layers[val][j+1][i+1] = 1 + n5 * 2 + n8 * 4 + n7 * 8 ;
					if(!n4){ layers[val][j+1][i  ] = 0 + 2 + n7 * 4 + n6 * 8 ; }
					if(!n2){ layers[val][j  ][i+1] = 0 + n3*2 + n5 * 4 + 8 ; }
					if(!n1){ layers[val][j  ][i  ] = 0 + n2*2 + 4 + n4 * 8 ; }
					
				}// End of i loop
			}// End of j loop
			
			return layers;
		},// End of layering()
		
		// 2. Layer separation and edge detection
		// Edge node types ( ▓: this layer or 1; ░: not this layer or 0 )
		// 12  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓
		// 48  ░░  ░░  ░░  ░░  ░▓  ░▓  ░▓  ░▓  ▓░  ▓░  ▓░  ▓░  ▓▓  ▓▓  ▓▓  ▓▓
		//     0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
		this.layeringstep = function(ii,cnum){
			// Creating layers for each indexed color in arr
			var layer = [], ah = ii.array.length, aw = ii.array[0].length, i, j;
			
			// Create layer
			for(j=0; j<ah; j++){
				layer[j] = [];
				for(i=0; i<aw; i++){
					layer[j][i]=0;
				}
			}
			
			// Looping through all pixels and calculating edge node type
			for(j=1; j<ah; j++){
				for(i=1; i<aw; i++){
					layer[j][i] =
						( ii.array[j-1][i-1]===cnum ? 1 : 0 ) +
						( ii.array[j-1][i]===cnum ? 2 : 0 ) +
						( ii.array[j][i-1]===cnum ? 8 : 0 ) +
						( ii.array[j][i]===cnum ? 4 : 0 )
					;
				}// End of i loop
			}// End of j loop
				
			return layer;
		},// End of layeringstep()
		
		// Point in polygon test
		this.pointinpoly = function( p, pa ){
			var isin=false;

			for(var i=0,j=pa.length-1; i<pa.length; j=i++){
				isin =
					( ((pa[i].y > p.y) !== (pa[j].y > p.y)) && (p.x < (pa[j].x - pa[i].x) * (p.y - pa[i].y) / (pa[j].y - pa[i].y) + pa[i].x) )
					? !isin : isin;
			}

			return isin;
		},
		
		// Lookup tables for pathscan
		// pathscan_combined_lookup[ arr[py][px] ][ dir ] = [nextarrpypx, nextdir, deltapx, deltapy];
		this.pathscan_combined_lookup = [
			[[-1,-1,-1,-1], [-1,-1,-1,-1], [-1,-1,-1,-1], [-1,-1,-1,-1]],// arr[py][px]===0 is invalid
			[[ 0, 1, 0,-1], [-1,-1,-1,-1], [-1,-1,-1,-1], [ 0, 2,-1, 0]],
			[[-1,-1,-1,-1], [-1,-1,-1,-1], [ 0, 1, 0,-1], [ 0, 0, 1, 0]],
			[[ 0, 0, 1, 0], [-1,-1,-1,-1], [ 0, 2,-1, 0], [-1,-1,-1,-1]],
			
			[[-1,-1,-1,-1], [ 0, 0, 1, 0], [ 0, 3, 0, 1], [-1,-1,-1,-1]],
			[[13, 3, 0, 1], [13, 2,-1, 0], [ 7, 1, 0,-1], [ 7, 0, 1, 0]],
			[[-1,-1,-1,-1], [ 0, 1, 0,-1], [-1,-1,-1,-1], [ 0, 3, 0, 1]],
			[[ 0, 3, 0, 1], [ 0, 2,-1, 0], [-1,-1,-1,-1], [-1,-1,-1,-1]],
			
			[[ 0, 3, 0, 1], [ 0, 2,-1, 0], [-1,-1,-1,-1], [-1,-1,-1,-1]],
			[[-1,-1,-1,-1], [ 0, 1, 0,-1], [-1,-1,-1,-1], [ 0, 3, 0, 1]],
			[[11, 1, 0,-1], [14, 0, 1, 0], [14, 3, 0, 1], [11, 2,-1, 0]],
			[[-1,-1,-1,-1], [ 0, 0, 1, 0], [ 0, 3, 0, 1], [-1,-1,-1,-1]],
			
			[[ 0, 0, 1, 0], [-1,-1,-1,-1], [ 0, 2,-1, 0], [-1,-1,-1,-1]],
			[[-1,-1,-1,-1], [-1,-1,-1,-1], [ 0, 1, 0,-1], [ 0, 0, 1, 0]],
			[[ 0, 1, 0,-1], [-1,-1,-1,-1], [-1,-1,-1,-1], [ 0, 2,-1, 0]],
			[[-1,-1,-1,-1], [-1,-1,-1,-1], [-1,-1,-1,-1], [-1,-1,-1,-1]]// arr[py][px]===15 is invalid
		],

		// 3. Walking through an edge node array, discarding edge node types 0 and 15 and creating paths from the rest.
		// Walk directions (dir): 0 > ; 1 ^ ; 2 < ; 3 v 
		this.pathscan = function( arr, pathomit ){
			var paths=[], pacnt=0, pcnt=0, px=0, py=0, w = arr[0].length, h = arr.length,
				dir=0, pathfinished=true, holepath=false, lookuprow;
			
			for(var j=0; j<h; j++){
				for(var i=0; i<w; i++){
					if( (arr[j][i] == 4) || ( arr[j][i] == 11) ){ // Other values are not valid
						
						// Init
						px = i; py = j;
						paths[pacnt] = {};
						paths[pacnt].points = [];
						paths[pacnt].boundingbox = [px,py,px,py];
						paths[pacnt].holechildren = [];
						pathfinished = false;
						pcnt=0;
						holepath = (arr[j][i]==11);
						dir = 1;

						// Path points loop
						while(!pathfinished){
							
							// New path point
							paths[pacnt].points[pcnt] = {};
							paths[pacnt].points[pcnt].x = px-1;
							paths[pacnt].points[pcnt].y = py-1;
							paths[pacnt].points[pcnt].t = arr[py][px];
							
							// Bounding box
							if( (px-1) < paths[pacnt].boundingbox[0] ){ paths[pacnt].boundingbox[0] = px-1; }
							if( (px-1) > paths[pacnt].boundingbox[2] ){ paths[pacnt].boundingbox[2] = px-1; }
							if( (py-1) < paths[pacnt].boundingbox[1] ){ paths[pacnt].boundingbox[1] = py-1; }
							if( (py-1) > paths[pacnt].boundingbox[3] ){ paths[pacnt].boundingbox[3] = py-1; }
							
							// Next: look up the replacement, direction and coordinate changes = clear this cell, turn if required, walk forward
							lookuprow = _this.pathscan_combined_lookup[ arr[py][px] ][ dir ];
							arr[py][px] = lookuprow[0]; dir = lookuprow[1]; px += lookuprow[2]; py += lookuprow[3];

							// Close path
							if( (px-1 === paths[pacnt].points[0].x ) && ( py-1 === paths[pacnt].points[0].y ) ){
								pathfinished = true;
								
								// Discarding paths shorter than pathomit
								if( paths[pacnt].points.length < pathomit ){
									paths.pop();
								}else {
								
									paths[pacnt].isholepath = holepath ? true : false;
									
									// Finding the parent shape for this hole
									if(holepath){
										
										var parentidx = 0, parentbbox = [-1,-1,w+1,h+1];
										for(var parentcnt=0; parentcnt < pacnt; parentcnt++){
											if( (!paths[parentcnt].isholepath) &&
												_this.boundingboxincludes( paths[parentcnt].boundingbox , paths[pacnt].boundingbox ) &&
												_this.boundingboxincludes( parentbbox , paths[parentcnt].boundingbox ) &&
												_this.pointinpoly( paths[pacnt].points[0], paths[parentcnt].points )
											){
												parentidx = parentcnt;
												parentbbox = paths[parentcnt].boundingbox;
											}
										}
										
										paths[parentidx].holechildren.push( pacnt );
										
									}// End of holepath parent finding
									
									pacnt++;
								
								}
								
							}// End of Close path
							
							pcnt++;
							
						}// End of Path points loop
						
					}// End of Follow path
					
				}// End of i loop
			}// End of j loop
			
			return paths;
		},// End of pathscan()
		
		this.boundingboxincludes = function( parentbbox, childbbox ){
			return ( ( parentbbox[0] < childbbox[0] ) && ( parentbbox[1] < childbbox[1] ) && ( parentbbox[2] > childbbox[2] ) && ( parentbbox[3] > childbbox[3] ) );
		},// End of boundingboxincludes()
		
		// 3. Batch pathscan
		this.batchpathscan = function( layers, pathomit ){
			var bpaths = [];
			for(var k in layers){
				if(!layers.hasOwnProperty(k)){ continue; }
				bpaths[k] = _this.pathscan( layers[k], pathomit );
			}
			return bpaths;
		},
		
		// 4. interpollating between path points for nodes with 8 directions ( East, SouthEast, S, SW, W, NW, N, NE )
		this.internodes = function( paths, options ){
			var ins = [], palen=0, nextidx=0, nextidx2=0, previdx=0, previdx2=0, pacnt, pcnt;
			
			// paths loop
			for(pacnt=0; pacnt<paths.length; pacnt++){
				
				ins[pacnt] = {};
				ins[pacnt].points = [];
				ins[pacnt].boundingbox = paths[pacnt].boundingbox;
				ins[pacnt].holechildren = paths[pacnt].holechildren;
				ins[pacnt].isholepath = paths[pacnt].isholepath;
				palen = paths[pacnt].points.length;
				
				// pathpoints loop
				for(pcnt=0; pcnt<palen; pcnt++){
				
					// next and previous point indexes
					nextidx = (pcnt+1)%palen; nextidx2 = (pcnt+2)%palen; previdx = (pcnt-1+palen)%palen; previdx2 = (pcnt-2+palen)%palen;
					
					// right angle enhance
					if( options.rightangleenhance && _this.testrightangle( paths[pacnt], previdx2, previdx, pcnt, nextidx, nextidx2 ) ){
						
						// Fix previous direction
						if(ins[pacnt].points.length > 0){
							ins[pacnt].points[ ins[pacnt].points.length-1 ].linesegment = _this.getdirection(
									ins[pacnt].points[ ins[pacnt].points.length-1 ].x,
									ins[pacnt].points[ ins[pacnt].points.length-1 ].y,
									paths[pacnt].points[pcnt].x,
									paths[pacnt].points[pcnt].y
								);
						}
						
						// This corner point
						ins[pacnt].points.push({
							x : paths[pacnt].points[pcnt].x,
							y : paths[pacnt].points[pcnt].y,
							linesegment : _this.getdirection(
									paths[pacnt].points[pcnt].x,
									paths[pacnt].points[pcnt].y,
									(( paths[pacnt].points[pcnt].x + paths[pacnt].points[nextidx].x ) /2),
									(( paths[pacnt].points[pcnt].y + paths[pacnt].points[nextidx].y ) /2)
								)
						});
						
					}// End of right angle enhance
					
					// interpolate between two path points
					ins[pacnt].points.push({
						x : (( paths[pacnt].points[pcnt].x + paths[pacnt].points[nextidx].x ) /2),
						y : (( paths[pacnt].points[pcnt].y + paths[pacnt].points[nextidx].y ) /2),
						linesegment : _this.getdirection(
								(( paths[pacnt].points[pcnt].x + paths[pacnt].points[nextidx].x ) /2),
								(( paths[pacnt].points[pcnt].y + paths[pacnt].points[nextidx].y ) /2),
								(( paths[pacnt].points[nextidx].x + paths[pacnt].points[nextidx2].x ) /2),
								(( paths[pacnt].points[nextidx].y + paths[pacnt].points[nextidx2].y ) /2)
							)
					});
					
				}// End of pathpoints loop
							
			}// End of paths loop
			
			return ins;
		},// End of internodes()
		
		this.testrightangle = function( path, idx1, idx2, idx3, idx4, idx5 ){
			return ( (( path.points[idx3].x === path.points[idx1].x) &&
					  ( path.points[idx3].x === path.points[idx2].x) &&
					  ( path.points[idx3].y === path.points[idx4].y) &&
					  ( path.points[idx3].y === path.points[idx5].y)
					 ) ||
					 (( path.points[idx3].y === path.points[idx1].y) &&
					  ( path.points[idx3].y === path.points[idx2].y) &&
					  ( path.points[idx3].x === path.points[idx4].x) &&
					  ( path.points[idx3].x === path.points[idx5].x)
					 )
			);
		},// End of testrightangle()
		
		this.getdirection = function( x1, y1, x2, y2 ){
			var val = 8;
			if(x1 < x2){
				if     (y1 < y2){ val = 1; }// SouthEast
				else if(y1 > y2){ val = 7; }// NE
				else            { val = 0; }// E
			}else if(x1 > x2){
				if     (y1 < y2){ val = 3; }// SW
				else if(y1 > y2){ val = 5; }// NW
				else            { val = 4; }// W
			}else {
				if     (y1 < y2){ val = 2; }// S
				else if(y1 > y2){ val = 6; }// N
				else            { val = 8; }// center, this should not happen
			}
			return val;
		},// End of getdirection()
		
		// 4. Batch interpollation
		this.batchinternodes = function( bpaths, options ){
			var binternodes = [];
			for (var k in bpaths) {
				if(!bpaths.hasOwnProperty(k)){ continue; }
				binternodes[k] = _this.internodes(bpaths[k], options);
			}
			return binternodes;
		},
		
		// 5. tracepath() : recursively trying to fit straight and quadratic spline segments on the 8 direction internode path
		
		// 5.1. Find sequences of points with only 2 segment types
		// 5.2. Fit a straight line on the sequence
		// 5.3. If the straight line fails (distance error > ltres), find the point with the biggest error
		// 5.4. Fit a quadratic spline through errorpoint (project this to get controlpoint), then measure errors on every point in the sequence
		// 5.5. If the spline fails (distance error > qtres), find the point with the biggest error, set splitpoint = fitting point
		// 5.6. Split sequence and recursively apply 5.2. - 5.6. to startpoint-splitpoint and splitpoint-endpoint sequences
		
		this.tracepath = function( path, ltres, qtres ){
			var pcnt=0, segtype1, segtype2, seqend, smp = {};
			smp.segments = [];
			smp.boundingbox = path.boundingbox;
			smp.holechildren = path.holechildren;
			smp.isholepath = path.isholepath;
			
			while(pcnt < path.points.length){
				// 5.1. Find sequences of points with only 2 segment types
				segtype1 = path.points[pcnt].linesegment; segtype2 = -1; seqend=pcnt+1;
				while(
					((path.points[seqend].linesegment === segtype1) || (path.points[seqend].linesegment === segtype2) || (segtype2 === -1))
					&& (seqend < path.points.length-1) ){
					
					if((path.points[seqend].linesegment!==segtype1) && (segtype2===-1)){ segtype2 = path.points[seqend].linesegment; }
					seqend++;
					
				}
				if(seqend === path.points.length-1){ seqend = 0; }

				// 5.2. - 5.6. Split sequence and recursively apply 5.2. - 5.6. to startpoint-splitpoint and splitpoint-endpoint sequences
				smp.segments = smp.segments.concat( _this.fitseq(path, ltres, qtres, pcnt, seqend) );
				
				// forward pcnt;
				if(seqend>0){ pcnt = seqend; }else { pcnt = path.points.length; }
				
			}// End of pcnt loop
			
			return smp;
		},// End of tracepath()
			
		// 5.2. - 5.6. recursively fitting a straight or quadratic line segment on this sequence of path nodes,
		// called from tracepath()
		this.fitseq = function( path, ltres, qtres, seqstart, seqend ){
			// return if invalid seqend
			if( (seqend>path.points.length) || (seqend<0) ){ return []; }
			// variables
			var errorpoint=seqstart, errorval=0, curvepass=true, px, py, dist2;
			var tl = (seqend-seqstart); if(tl<0){ tl += path.points.length; }
			var vx = (path.points[seqend].x-path.points[seqstart].x) / tl,
				vy = (path.points[seqend].y-path.points[seqstart].y) / tl;
			
			// 5.2. Fit a straight line on the sequence
			var pcnt = (seqstart+1) % path.points.length, pl;
			while(pcnt != seqend){
				pl = pcnt-seqstart; if(pl<0){ pl += path.points.length; }
				px = path.points[seqstart].x + vx * pl; py = path.points[seqstart].y + vy * pl;
				dist2 = (path.points[pcnt].x-px)*(path.points[pcnt].x-px) + (path.points[pcnt].y-py)*(path.points[pcnt].y-py);
				if(dist2>ltres){curvepass=false;}
				if(dist2>errorval){ errorpoint=pcnt; errorval=dist2; }
				pcnt = (pcnt+1)%path.points.length;
			}
			// return straight line if fits
			if(curvepass){ return [{ type:'L', x1:path.points[seqstart].x, y1:path.points[seqstart].y, x2:path.points[seqend].x, y2:path.points[seqend].y }]; }
			
			// 5.3. If the straight line fails (distance error>ltres), find the point with the biggest error
			var fitpoint = errorpoint; curvepass = true; errorval = 0;
			
			// 5.4. Fit a quadratic spline through this point, measure errors on every point in the sequence
			// helpers and projecting to get control point
			var t=(fitpoint-seqstart)/tl, t1=(1-t)*(1-t), t2=2*(1-t)*t, t3=t*t;
			var cpx = (t1*path.points[seqstart].x + t3*path.points[seqend].x - path.points[fitpoint].x)/-t2 ,
				cpy = (t1*path.points[seqstart].y + t3*path.points[seqend].y - path.points[fitpoint].y)/-t2 ;
			
			// Check every point
			pcnt = seqstart+1;
			while(pcnt != seqend){
				t=(pcnt-seqstart)/tl; t1=(1-t)*(1-t); t2=2*(1-t)*t; t3=t*t;
				px = t1 * path.points[seqstart].x + t2 * cpx + t3 * path.points[seqend].x;
				py = t1 * path.points[seqstart].y + t2 * cpy + t3 * path.points[seqend].y;
				
				dist2 = (path.points[pcnt].x-px)*(path.points[pcnt].x-px) + (path.points[pcnt].y-py)*(path.points[pcnt].y-py);
				
				if(dist2>qtres){curvepass=false;}
				if(dist2>errorval){ errorpoint=pcnt; errorval=dist2; }
				pcnt = (pcnt+1)%path.points.length;
			}
			// return spline if fits
			if(curvepass){ return [{ type:'Q', x1:path.points[seqstart].x, y1:path.points[seqstart].y, x2:cpx, y2:cpy, x3:path.points[seqend].x, y3:path.points[seqend].y }]; }
			// 5.5. If the spline fails (distance error>qtres), find the point with the biggest error
			var splitpoint = fitpoint; // Earlier: Math.floor((fitpoint + errorpoint)/2);
			
			// 5.6. Split sequence and recursively apply 5.2. - 5.6. to startpoint-splitpoint and splitpoint-endpoint sequences
			return _this.fitseq( path, ltres, qtres, seqstart, splitpoint ).concat(
					_this.fitseq( path, ltres, qtres, splitpoint, seqend ) );
			
		},// End of fitseq()
		
		// 5. Batch tracing paths
		this.batchtracepaths = function(internodepaths,ltres,qtres){
			var btracedpaths = [];
			for(var k in internodepaths){
				if(!internodepaths.hasOwnProperty(k)){ continue; }
				btracedpaths.push( _this.tracepath(internodepaths[k],ltres,qtres) );
			}
			return btracedpaths;
		},
		
		// 5. Batch tracing layers
		this.batchtracelayers = function(binternodes, ltres, qtres){
			var btbis = [];
			for(var k in binternodes){
				if(!binternodes.hasOwnProperty(k)){ continue; }
				btbis[k] = _this.batchtracepaths(binternodes[k], ltres, qtres);
			}
			return btbis;
		},
		
		////////////////////////////////////////////////////////////
		//
		//  SVG Drawing functions
		//
		////////////////////////////////////////////////////////////
		
		// Rounding to given decimals https://stackoverflow.com/questions/11832914/round-to-at-most-2-decimal-places-in-javascript
		this.roundtodec = function(val,places){ return +val.toFixed(places); },
		
		// Getting SVG path element string from a traced path
		this.svgpathstring = function( tracedata, lnum, pathnum, options ){
			
			var layer = tracedata.layers[lnum], smp = layer[pathnum], str='', pcnt;
			
			// Line filter
			if(options.linefilter && (smp.segments.length < 3)){ return str; }
			
			// Starting path element, desc contains layer and path number
			str = '<path '+
				( options.desc ? ('desc="l '+lnum+' p '+pathnum+'" ') : '' ) +
				_this.tosvgcolorstr(tracedata.palette[lnum], options) +
				'd="';
			
			// Creating non-hole path string
			if( options.roundcoords === -1 ){
				str += 'M '+ smp.segments[0].x1 * options.scale +' '+ smp.segments[0].y1 * options.scale +' ';
				for(pcnt=0; pcnt<smp.segments.length; pcnt++){
					str += smp.segments[pcnt].type +' '+ smp.segments[pcnt].x2 * options.scale +' '+ smp.segments[pcnt].y2 * options.scale +' ';
					if(smp.segments[pcnt].hasOwnProperty('x3')){
						str += smp.segments[pcnt].x3 * options.scale +' '+ smp.segments[pcnt].y3 * options.scale +' ';
					}
				}
				str += 'Z ';
			}else {
				str += 'M '+ _this.roundtodec( smp.segments[0].x1 * options.scale, options.roundcoords ) +' '+ _this.roundtodec( smp.segments[0].y1 * options.scale, options.roundcoords ) +' ';
				for(pcnt=0; pcnt<smp.segments.length; pcnt++){
					str += smp.segments[pcnt].type +' '+ _this.roundtodec( smp.segments[pcnt].x2 * options.scale, options.roundcoords ) +' '+ _this.roundtodec( smp.segments[pcnt].y2 * options.scale, options.roundcoords ) +' ';
					if(smp.segments[pcnt].hasOwnProperty('x3')){
						str += _this.roundtodec( smp.segments[pcnt].x3 * options.scale, options.roundcoords ) +' '+ _this.roundtodec( smp.segments[pcnt].y3 * options.scale, options.roundcoords ) +' ';
					}
				}
				str += 'Z ';
			}// End of creating non-hole path string
			
			// Hole children
			for( var hcnt=0; hcnt < smp.holechildren.length; hcnt++){
				var hsmp = layer[ smp.holechildren[hcnt] ];
				// Creating hole path string
				if( options.roundcoords === -1 ){
					
					if(hsmp.segments[ hsmp.segments.length-1 ].hasOwnProperty('x3')){
						str += 'M '+ hsmp.segments[ hsmp.segments.length-1 ].x3 * options.scale +' '+ hsmp.segments[ hsmp.segments.length-1 ].y3 * options.scale +' ';
					}else {
						str += 'M '+ hsmp.segments[ hsmp.segments.length-1 ].x2 * options.scale +' '+ hsmp.segments[ hsmp.segments.length-1 ].y2 * options.scale +' ';
					}
					
					for(pcnt = hsmp.segments.length-1; pcnt >= 0; pcnt--){
						str += hsmp.segments[pcnt].type +' ';
						if(hsmp.segments[pcnt].hasOwnProperty('x3')){
							str += hsmp.segments[pcnt].x2 * options.scale +' '+ hsmp.segments[pcnt].y2 * options.scale +' ';
						}
						
						str += hsmp.segments[pcnt].x1 * options.scale +' '+ hsmp.segments[pcnt].y1 * options.scale +' ';
					}
					
				}else {
					
					if(hsmp.segments[ hsmp.segments.length-1 ].hasOwnProperty('x3')){
						str += 'M '+ _this.roundtodec( hsmp.segments[ hsmp.segments.length-1 ].x3 * options.scale ) +' '+ _this.roundtodec( hsmp.segments[ hsmp.segments.length-1 ].y3 * options.scale ) +' ';
					}else {
						str += 'M '+ _this.roundtodec( hsmp.segments[ hsmp.segments.length-1 ].x2 * options.scale ) +' '+ _this.roundtodec( hsmp.segments[ hsmp.segments.length-1 ].y2 * options.scale ) +' ';
					}
					
					for(pcnt = hsmp.segments.length-1; pcnt >= 0; pcnt--){
						str += hsmp.segments[pcnt].type +' ';
						if(hsmp.segments[pcnt].hasOwnProperty('x3')){
							str += _this.roundtodec( hsmp.segments[pcnt].x2 * options.scale ) +' '+ _this.roundtodec( hsmp.segments[pcnt].y2 * options.scale ) +' ';
						}
						str += _this.roundtodec( hsmp.segments[pcnt].x1 * options.scale ) +' '+ _this.roundtodec( hsmp.segments[pcnt].y1 * options.scale ) +' ';
					}
					
					
				}// End of creating hole path string
				
				str += 'Z '; // Close path
				
			}// End of holepath check
			
			// Closing path element
			str += '" />';
			
			// Rendering control points
			if(options.lcpr || options.qcpr){
				for(pcnt=0; pcnt<smp.segments.length; pcnt++){
					if( smp.segments[pcnt].hasOwnProperty('x3') && options.qcpr ){
						str += '<circle cx="'+ smp.segments[pcnt].x2 * options.scale +'" cy="'+ smp.segments[pcnt].y2 * options.scale +'" r="'+ options.qcpr +'" fill="cyan" stroke-width="'+ options.qcpr * 0.2 +'" stroke="black" />';
						str += '<circle cx="'+ smp.segments[pcnt].x3 * options.scale +'" cy="'+ smp.segments[pcnt].y3 * options.scale +'" r="'+ options.qcpr +'" fill="white" stroke-width="'+ options.qcpr * 0.2 +'" stroke="black" />';
						str += '<line x1="'+ smp.segments[pcnt].x1 * options.scale +'" y1="'+ smp.segments[pcnt].y1 * options.scale +'" x2="'+ smp.segments[pcnt].x2 * options.scale +'" y2="'+ smp.segments[pcnt].y2 * options.scale +'" stroke-width="'+ options.qcpr * 0.2 +'" stroke="cyan" />';
						str += '<line x1="'+ smp.segments[pcnt].x2 * options.scale +'" y1="'+ smp.segments[pcnt].y2 * options.scale +'" x2="'+ smp.segments[pcnt].x3 * options.scale +'" y2="'+ smp.segments[pcnt].y3 * options.scale +'" stroke-width="'+ options.qcpr * 0.2 +'" stroke="cyan" />';
					}
					if( (!smp.segments[pcnt].hasOwnProperty('x3')) && options.lcpr){
						str += '<circle cx="'+ smp.segments[pcnt].x2 * options.scale +'" cy="'+ smp.segments[pcnt].y2 * options.scale +'" r="'+ options.lcpr +'" fill="white" stroke-width="'+ options.lcpr * 0.2 +'" stroke="black" />';
					}
				}
				
				// Hole children control points
				for( var hcnt=0; hcnt < smp.holechildren.length; hcnt++){
					var hsmp = layer[ smp.holechildren[hcnt] ];
					for(pcnt=0; pcnt<hsmp.segments.length; pcnt++){
						if( hsmp.segments[pcnt].hasOwnProperty('x3') && options.qcpr ){
							str += '<circle cx="'+ hsmp.segments[pcnt].x2 * options.scale +'" cy="'+ hsmp.segments[pcnt].y2 * options.scale +'" r="'+ options.qcpr +'" fill="cyan" stroke-width="'+ options.qcpr * 0.2 +'" stroke="black" />';
							str += '<circle cx="'+ hsmp.segments[pcnt].x3 * options.scale +'" cy="'+ hsmp.segments[pcnt].y3 * options.scale +'" r="'+ options.qcpr +'" fill="white" stroke-width="'+ options.qcpr * 0.2 +'" stroke="black" />';
							str += '<line x1="'+ hsmp.segments[pcnt].x1 * options.scale +'" y1="'+ hsmp.segments[pcnt].y1 * options.scale +'" x2="'+ hsmp.segments[pcnt].x2 * options.scale +'" y2="'+ hsmp.segments[pcnt].y2 * options.scale +'" stroke-width="'+ options.qcpr * 0.2 +'" stroke="cyan" />';
							str += '<line x1="'+ hsmp.segments[pcnt].x2 * options.scale +'" y1="'+ hsmp.segments[pcnt].y2 * options.scale +'" x2="'+ hsmp.segments[pcnt].x3 * options.scale +'" y2="'+ hsmp.segments[pcnt].y3 * options.scale +'" stroke-width="'+ options.qcpr * 0.2 +'" stroke="cyan" />';
						}
						if( (!hsmp.segments[pcnt].hasOwnProperty('x3')) && options.lcpr){
							str += '<circle cx="'+ hsmp.segments[pcnt].x2 * options.scale +'" cy="'+ hsmp.segments[pcnt].y2 * options.scale +'" r="'+ options.lcpr +'" fill="white" stroke-width="'+ options.lcpr * 0.2 +'" stroke="black" />';
						}
					}
				}
			}// End of Rendering control points
				
			return str;
			
		},// End of svgpathstring()
		
		// Converting tracedata to an SVG string
		this.getsvgstring = function( tracedata, options ){
			
			options = _this.checkoptions(options);
			
			var w = tracedata.width * options.scale, h = tracedata.height * options.scale;
			
			// SVG start
			var svgstr = '<svg ' + (options.viewbox ? ('viewBox="0 0 '+w+' '+h+'" ') : ('width="'+w+'" height="'+h+'" ')) +
				'version="1.1" xmlns="http://www.w3.org/2000/svg" desc="Created with imagetracer.js version '+_this.versionnumber+'" >';

			// Drawing: Layers and Paths loops
			for(var lcnt=0; lcnt < tracedata.layers.length; lcnt++){
				for(var pcnt=0; pcnt < tracedata.layers[lcnt].length; pcnt++){
					
					// Adding SVG <path> string
					if( !tracedata.layers[lcnt][pcnt].isholepath ){
						svgstr += _this.svgpathstring( tracedata, lcnt, pcnt, options );
					}
						
				}// End of paths loop
			}// End of layers loop
			
			// SVG End
			svgstr+='</svg>';
			
			return svgstr;
			
		},// End of getsvgstring()
		
		// Comparator for numeric Array.sort
		this.compareNumbers = function(a,b){ return a - b; },
		
		// Convert color object to rgba string
		this.torgbastr = function(c){ return 'rgba('+c.r+','+c.g+','+c.b+','+c.a+')'; },
		
		// Convert color object to SVG color string
		this.tosvgcolorstr = function(c, options){
			return 'fill="rgb('+c.r+','+c.g+','+c.b+')" stroke="rgb('+c.r+','+c.g+','+c.b+')" stroke-width="'+options.strokewidth+'" opacity="'+c.a/255.0+'" ';
		},
		
		// Helper function: Appending an <svg> element to a container from an svgstring
		this.appendSVGString = function(svgstr,parentid){
			var div;
			if(parentid){
				div = document.getElementById(parentid);
				if(!div){
					div = document.createElement('div');
					div.id = parentid;
					document.body.appendChild(div);
				}
			}else {
				div = document.createElement('div');
				document.body.appendChild(div);
			}
			div.innerHTML += svgstr;
		},
		
		////////////////////////////////////////////////////////////
		//
		//  Canvas functions
		//
		////////////////////////////////////////////////////////////
		
		// Gaussian kernels for blur
		this.gks = [ [0.27901,0.44198,0.27901], [0.135336,0.228569,0.272192,0.228569,0.135336], [0.086776,0.136394,0.178908,0.195843,0.178908,0.136394,0.086776],
		             [0.063327,0.093095,0.122589,0.144599,0.152781,0.144599,0.122589,0.093095,0.063327], [0.049692,0.069304,0.089767,0.107988,0.120651,0.125194,0.120651,0.107988,0.089767,0.069304,0.049692] ],
		
		// Selective Gaussian blur for preprocessing
		this.blur = function(imgd,radius,delta){
			var i,j,k,d,idx,racc,gacc,bacc,aacc,wacc;
			
			// new ImageData
			var imgd2 = { width:imgd.width, height:imgd.height, data:[] };
			
			// radius and delta limits, this kernel
			radius = Math.floor(radius); if(radius<1){ return imgd; } if(radius>5){ radius = 5; } delta = Math.abs( delta ); if(delta>1024){ delta = 1024; }
			var thisgk = _this.gks[radius-1];
			
			// loop through all pixels, horizontal blur
			for( j=0; j < imgd.height; j++ ){
				for( i=0; i < imgd.width; i++ ){

					racc = 0; gacc = 0; bacc = 0; aacc = 0; wacc = 0;
					// gauss kernel loop
					for( k = -radius; k < radius+1; k++){
						// add weighted color values
						if( (i+k > 0) && (i+k < imgd.width) ){
							idx = (j*imgd.width+i+k)*4;
							racc += imgd.data[idx  ] * thisgk[k+radius];
							gacc += imgd.data[idx+1] * thisgk[k+radius];
							bacc += imgd.data[idx+2] * thisgk[k+radius];
							aacc += imgd.data[idx+3] * thisgk[k+radius];
							wacc += thisgk[k+radius];
						}
					}
					// The new pixel
					idx = (j*imgd.width+i)*4;
					imgd2.data[idx  ] = Math.floor(racc / wacc);
					imgd2.data[idx+1] = Math.floor(gacc / wacc);
					imgd2.data[idx+2] = Math.floor(bacc / wacc);
					imgd2.data[idx+3] = Math.floor(aacc / wacc);
					
				}// End of width loop
			}// End of horizontal blur
			
			// copying the half blurred imgd2
			var himgd = new Uint8ClampedArray(imgd2.data);
			
			// loop through all pixels, vertical blur
			for( j=0; j < imgd.height; j++ ){
				for( i=0; i < imgd.width; i++ ){

					racc = 0; gacc = 0; bacc = 0; aacc = 0; wacc = 0;
					// gauss kernel loop
					for( k = -radius; k < radius+1; k++){
						// add weighted color values
						if( (j+k > 0) && (j+k < imgd.height) ){
							idx = ((j+k)*imgd.width+i)*4;
							racc += himgd[idx  ] * thisgk[k+radius];
							gacc += himgd[idx+1] * thisgk[k+radius];
							bacc += himgd[idx+2] * thisgk[k+radius];
							aacc += himgd[idx+3] * thisgk[k+radius];
							wacc += thisgk[k+radius];
						}
					}
					// The new pixel
					idx = (j*imgd.width+i)*4;
					imgd2.data[idx  ] = Math.floor(racc / wacc);
					imgd2.data[idx+1] = Math.floor(gacc / wacc);
					imgd2.data[idx+2] = Math.floor(bacc / wacc);
					imgd2.data[idx+3] = Math.floor(aacc / wacc);
					
				}// End of width loop
			}// End of vertical blur
			
			// Selective blur: loop through all pixels
			for( j=0; j < imgd.height; j++ ){
				for( i=0; i < imgd.width; i++ ){
					
					idx = (j*imgd.width+i)*4;
					// d is the difference between the blurred and the original pixel
					d = Math.abs(imgd2.data[idx  ] - imgd.data[idx  ]) + Math.abs(imgd2.data[idx+1] - imgd.data[idx+1]) +
						Math.abs(imgd2.data[idx+2] - imgd.data[idx+2]) + Math.abs(imgd2.data[idx+3] - imgd.data[idx+3]);
					// selective blur: if d>delta, put the original pixel back
					if(d>delta){
						imgd2.data[idx  ] = imgd.data[idx  ];
						imgd2.data[idx+1] = imgd.data[idx+1];
						imgd2.data[idx+2] = imgd.data[idx+2];
						imgd2.data[idx+3] = imgd.data[idx+3];
					}
				}
			}// End of Selective blur
			
			return imgd2;
			
		},// End of blur()
		
		// Helper function: loading an image from a URL, then executing callback with canvas as argument
		this.loadImage = function(url,callback,options){
			var img = new Image();
			if(options && options.corsenabled){ img.crossOrigin = 'Anonymous'; }
			img.onload = function(){
				var canvas = document.createElement('canvas');
				canvas.width = img.width;
				canvas.height = img.height;
				var context = canvas.getContext('2d');
				context.drawImage(img,0,0);
				callback(canvas);
			};
			img.src = url;
		},
		
		// Helper function: getting ImageData from a canvas
		this.getImgdata = function(canvas){
			var context = canvas.getContext('2d');
			return context.getImageData(0,0,canvas.width,canvas.height);
		},
		
		// Special palette to use with drawlayers()
		this.specpalette = [
			{r:0,g:0,b:0,a:255}, {r:128,g:128,b:128,a:255}, {r:0,g:0,b:128,a:255}, {r:64,g:64,b:128,a:255},
			{r:192,g:192,b:192,a:255}, {r:255,g:255,b:255,a:255}, {r:128,g:128,b:192,a:255}, {r:0,g:0,b:192,a:255},
			{r:128,g:0,b:0,a:255}, {r:128,g:64,b:64,a:255}, {r:128,g:0,b:128,a:255}, {r:168,g:168,b:168,a:255},
			{r:192,g:128,b:128,a:255}, {r:192,g:0,b:0,a:255}, {r:255,g:255,b:255,a:255}, {r:0,g:128,b:0,a:255}
		],
		
		// Helper function: Drawing all edge node layers into a container
		this.drawLayers = function(layers,palette,scale,parentid){
			scale = scale||1;
			var w,h,i,j,k;
			
			// Preparing container
			var div;
			if(parentid){
				div = document.getElementById(parentid);
				if(!div){
					div = document.createElement('div');
					div.id = parentid;
					document.body.appendChild(div);
				}
			}else {
				div = document.createElement('div');
				document.body.appendChild(div);
			}
			
			// Layers loop
			for (k in layers) {
				if(!layers.hasOwnProperty(k)){ continue; }
				
				// width, height
				w=layers[k][0].length; h=layers[k].length;
				
				// Creating new canvas for every layer
				var canvas = document.createElement('canvas'); canvas.width=w*scale; canvas.height=h*scale;
				var context = canvas.getContext('2d');
				
				// Drawing
				for(j=0; j<h; j++){
					for(i=0; i<w; i++){
						context.fillStyle = _this.torgbastr(palette[ layers[k][j][i]%palette.length ]);
						context.fillRect(i*scale,j*scale,scale,scale);
					}
				}
				
				// Appending canvas to container
				div.appendChild(canvas);
			}// End of Layers loop
		}// End of drawlayers
		
		;// End of function list
		
	}// End of ImageTracer object

	// export as AMD module / Node module / browser or worker variable
	{
		module.exports = new ImageTracer();
	}

	})();
	}(imagetracer_v1_2_6));

	var ImageTracer = imagetracer_v1_2_6.exports;

	// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.

	class SmoothMosaicPlugin {

	    constructor(rows, cols) {
	        this._scaledGraphicsLayer = null;
	        this._doubleHeightCellsInRowAbove = new Set(); // keep track of double height cells to avoid clearing bottom half on graphics canvas
	        this._backgroundColoursInRowAbove = [];

	        const canvas = document.createElement('canvas');
	        this._canvasCtx = canvas.getContext('2d');
	        this._canvasCtx.width = cols * 2;
	        this._canvasCtx.height = rows * 3;
	        canvas.width = this._canvasCtx.width;
	        canvas.height = this._canvasCtx.height;
	        canvas.style.width = canvas.width * 2 + 'px';
	        canvas.style.height = canvas.height * 2 + 'px';
	        this._canvasEl = canvas;
	        // document.querySelector('#canvas').appendChild(canvas); // for debugging

	        console.debug('SmoothMosaicPlugin constructed');
	    }

	    static registerWithView(view) {
	        const rows = view.constructor.ROWS;
	        const cols = view.constructor.COLS;
	        const instance = new SmoothMosaicPlugin(rows, cols);
	        const regResponse = view.registerPlugin(SmoothMosaicPlugin.name, {
	            renderBackground: instance.renderBackground.bind(instance),
	            renderMosaic: instance.renderMosaic.bind(instance),
	            endOfPageUpdate: instance.endOfUpdate.bind(instance),
	            clearCellsForRow: instance.clearRowCells.bind(instance)
	        });
	        instance._lookupColour = regResponse.lookupColour;    // returns #rgb
	        instance._isDoubleHeight = regResponse.isDoubleHeight;
	        instance._isDoubleWidth = regResponse.isDoubleWidth;
	        instance._isDoubleSize = regResponse.isDoubleSize;
	        instance._isSeparatedMosaic = regResponse.isSeparatedMosaic;
	        instance._svg = regResponse.createSVGOverlay();
	        instance._svg.attr('viewBox', `0 0 ${cols * 2 * 4} ${rows * 3 * 4}`);
	    }

	    _setSVGOverlay(markup) {
	        this._svg.node.innerHTML = markup;
	    }

	    renderBackground(rowIndex, cellIndex, size, bgColour) {
	        let bgWidth = 2;
	        let bgHeight = 3;
	        if (this._isDoubleHeight(size) || this._isDoubleSize(size)) {
	            bgHeight = 6;
	            this._doubleHeightCellsInRowAbove.add(cellIndex);
	        }
	        if (this._isDoubleSize(size)) {
	            this._doubleHeightCellsInRowAbove.add(cellIndex + 1);
	        }
	        if (this._isDoubleWidth(size) || this._isDoubleSize(size)) {
	            bgWidth = 4;
	        }
	        this._canvasCtx.clearRect(cellIndex * 2, rowIndex * 3, bgWidth, bgHeight);
	        // transparency is '7' so anti-aliasing uses correct bg colour and transpancy isn't rendered in scaled canvas
	        const colourFill = this._lookupColour(bgColour) + '7';
	        this._canvasCtx.fillStyle = colourFill;
	        this._canvasCtx.fillRect(cellIndex * 2, rowIndex * 3, bgWidth, bgHeight);
	        this._backgroundColoursInRowAbove[cellIndex] = colourFill;
	    }

	    // eslint-disable-next-line no-unused-vars
	    endOfUpdate(width, height) {
	        const targetCanvas = cloneCanvas(this._canvasEl);
	        hqx(targetCanvas, 4);

	        const imageData = ImageTracer.getImgdata(targetCanvas);
	        const traceOptions = {
	            colorsampling: 0,
	            pathomit: 13,
	        };
	        const SVGString = ImageTracer.imagedataToSVG(imageData, traceOptions);
	        this._setSVGOverlay(SVGString);
	    }

	    renderMosaic(row, col, cell, fill) {
	        if (this._isSeparatedMosaic(cell.type) || cell.flashing || cell.concealed) {
	            return false;
	        }
	        const sextants = cell.sextants;
	        if (!sextants.includes('1')) return true;
	        this._canvasCtx.fillStyle = fill;
	        for (let s = 0; s < 6; s++) {
	            if (sextants[s] == '1') {
	                let x, y, width = 1, height = 1;
	                if (this._isDoubleWidth(cell.size) || this._isDoubleSize(cell.size)) {
	                    x = ((s % 2)*2) + (col*2);
	                    width = 2;
	                } else
	                    x = (s % 2) + (col*2);

	                if (this._isDoubleHeight(cell.size) || this._isDoubleSize(cell.size)) {
	                    y =  (Math.floor(s/2)*2) + (row*3);
	                    height = 2;
	                } else
	                    y = Math.floor(s/2) + (row*3);

	                this._canvasCtx.fillRect(x, y, width, height);
	            }
	        }
	        return true;
	    }

	    clearRowCells(rowLength, rowNum) {
	        for (let colNum = 0; colNum < rowLength; colNum++) {
	            if (!this._doubleHeightCellsInRowAbove.has(colNum)) {
	                // fills in background on bottom half of row containing double height
	                this._canvasCtx.clearRect(colNum * 2, rowNum * 3, 2, 3);
	                this._canvasCtx.fillStyle = this._backgroundColoursInRowAbove[colNum];
	                this._canvasCtx.fillRect(colNum * 2, rowNum * 3, 2, 3);
	            }
	        }
	        this._doubleHeightCellsInRowAbove.clear();
	    }
	}

	function cloneCanvas(oldCanvas) {
	    const newCanvas = document.createElement('canvas');
	    const context = newCanvas.getContext('2d');
	    newCanvas.width = oldCanvas.width;
	    newCanvas.height = oldCanvas.height;
	    context.drawImage(oldCanvas, 0, 0);
	    return newCanvas;
	}

	exports.SmoothMosaicPlugin = SmoothMosaicPlugin;

	Object.defineProperty(exports, '__esModule', { value: true });

})));
