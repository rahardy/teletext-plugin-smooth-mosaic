// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
// SPDX-License-Identifier: ISC

import hqx from 'js-hqx';
import ImageTracer from 'imagetracerjs';

export class SmoothMosaicPlugin {

    constructor(rows, cols) {
        this._scaledGraphicsLayer = null;
        this._doubleHeightCellsInRowAbove = new Set(); // keep track of double height cells to avoid clearing bottom half on graphics canvas
        this._backgroundColoursInRowAbove = [];

        const canvas = document.createElement('canvas');
        this._canvasCtx = canvas.getContext('2d');
        this._canvasCtx.width = cols * 2;
        this._canvasCtx.height = rows * 3;
        canvas.width = this._canvasCtx.width;
        canvas.height = this._canvasCtx.height;
        canvas.style.width = canvas.width * 2 + 'px';
        canvas.style.height = canvas.height * 2 + 'px';
        this._canvasEl = canvas;
        // document.querySelector('#canvas').appendChild(canvas); // for debugging

        console.debug('SmoothMosaicPlugin constructed');
    }

    static registerWithView(view) {
        const rows = view.constructor.ROWS;
        const cols = view.constructor.COLS;
        const instance = new SmoothMosaicPlugin(rows, cols);
        const regResponse = view.registerPlugin(SmoothMosaicPlugin.name, {
            renderBackground: instance.renderBackground.bind(instance),
            renderMosaic: instance.renderMosaic.bind(instance),
            endOfPageUpdate: instance.endOfUpdate.bind(instance),
            clearCellsForRow: instance.clearRowCells.bind(instance)
        });
        instance._lookupColour = regResponse.lookupColour;    // returns #rgb
        instance._isDoubleHeight = regResponse.isDoubleHeight;
        instance._isDoubleWidth = regResponse.isDoubleWidth;
        instance._isDoubleSize = regResponse.isDoubleSize;
        instance._isSeparatedMosaic = regResponse.isSeparatedMosaic;
        instance._svg = regResponse.createSVGOverlay();
        instance._svg.attr('viewBox', `0 0 ${cols * 2 * 4} ${rows * 3 * 4}`);
    }

    _setSVGOverlay(markup) {
        this._svg.node.innerHTML = markup;
    }

    renderBackground(rowIndex, cellIndex, size, bgColour) {
        let bgWidth = 2;
        let bgHeight = 3;
        if (this._isDoubleHeight(size) || this._isDoubleSize(size)) {
            bgHeight = 6;
            this._doubleHeightCellsInRowAbove.add(cellIndex);
        }
        if (this._isDoubleSize(size)) {
            this._doubleHeightCellsInRowAbove.add(cellIndex + 1);
        }
        if (this._isDoubleWidth(size) || this._isDoubleSize(size)) {
            bgWidth = 4;
        }
        this._canvasCtx.clearRect(cellIndex * 2, rowIndex * 3, bgWidth, bgHeight);
        // transparency is '7' so anti-aliasing uses correct bg colour and transpancy isn't rendered in scaled canvas
        const colourFill = this._lookupColour(bgColour) + '7';
        this._canvasCtx.fillStyle = colourFill;
        this._canvasCtx.fillRect(cellIndex * 2, rowIndex * 3, bgWidth, bgHeight);
        this._backgroundColoursInRowAbove[cellIndex] = colourFill;
    }

    // eslint-disable-next-line no-unused-vars
    endOfUpdate(width, height) {
        const targetCanvas = cloneCanvas(this._canvasEl);
        hqx(targetCanvas, 4);

        const imageData = ImageTracer.getImgdata(targetCanvas);
        const traceOptions = {
            colorsampling: 0,
            pathomit: 13,
        };
        const SVGString = ImageTracer.imagedataToSVG(imageData, traceOptions);
        this._setSVGOverlay(SVGString);
    }

    renderMosaic(row, col, cell, fill) {
        if (this._isSeparatedMosaic(cell.type) || cell.flashing || cell.concealed) {
            return false;
        }
        const sextants = cell.sextants;
        if (!sextants.includes('1')) return true;
        this._canvasCtx.fillStyle = fill;
        for (let s = 0; s < 6; s++) {
            if (sextants[s] == '1') {
                let x, y, width = 1, height = 1;
                if (this._isDoubleWidth(cell.size) || this._isDoubleSize(cell.size)) {
                    x = ((s % 2)*2) + (col*2);
                    width = 2;
                } else
                    x = (s % 2) + (col*2);

                if (this._isDoubleHeight(cell.size) || this._isDoubleSize(cell.size)) {
                    y =  (Math.floor(s/2)*2) + (row*3);
                    height = 2;
                } else
                    y = Math.floor(s/2) + (row*3);

                this._canvasCtx.fillRect(x, y, width, height);
            }
        }
        return true;
    }

    clearRowCells(rowLength, rowNum) {
        for (let colNum = 0; colNum < rowLength; colNum++) {
            if (!this._doubleHeightCellsInRowAbove.has(colNum)) {
                // fills in background on bottom half of row containing double height
                this._canvasCtx.clearRect(colNum * 2, rowNum * 3, 2, 3);
                this._canvasCtx.fillStyle = this._backgroundColoursInRowAbove[colNum];
                this._canvasCtx.fillRect(colNum * 2, rowNum * 3, 2, 3);
            }
        }
        this._doubleHeightCellsInRowAbove.clear();
    }
}

function cloneCanvas(oldCanvas) {
    const newCanvas = document.createElement('canvas');
    const context = newCanvas.getContext('2d');
    newCanvas.width = oldCanvas.width;
    newCanvas.height = oldCanvas.height;
    context.drawImage(oldCanvas, 0, 0);
    return newCanvas;
}
