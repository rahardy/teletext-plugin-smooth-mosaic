<!--
SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
SPDX-License-Identifier: ISC
-->

This is a plugin for [@techandsoftware/teletext](https://www.npmjs.com/package/@techandsoftware/teletext), which uses a pixel art scaling algorithm to create a smooth rendition of teletext mosaic graphics.  The scaling algorithm used is [hqx](https://en.wikipedia.org/wiki/Hqx).

The following image shows a page from Ceefax with normal rendering and with smoothed graphics on the right:
![A Ceefax page with a map of the UK in teletext blocky format and with smoothed graphics](https://bitbucket.org/rahardy/teletext-plugin-smooth-mosaic/raw/master/docs/ceefax_normal_and_smooth_graphics.png)

# Demo

See https://teletextmoduledemo.tech-and-software.ltd.uk/ . Press 'p' or click the button marked with 'p' to load the plugin.

See the `docs` directory in the repo for an SVG created with the plugin.

# Using

## ES6 module

The easiest option is to use jsdelivr:

```javascript
const module = await import('https://cdn.jsdelivr.net/npm/@techandsoftware/teletext-plugin-smooth-mosaic@latest/dist/teletext-plugin-smooth-mosaic.min.js');

// teletext is the @techandsoftware/teletext instance
teletext.registerViewPlugin(module.SmoothMosaicPlugin);
```

If installing from npm, you'll also need to use tooling like rollup to resolve the dependency for your browser.

```javascript
import { SmoothMosaicPlugin } from '@techandsoftware/teletext-plugin-smooth-mosaic';
teletext.registerViewPlugin(SmoothMosaicPlugin);
```

As the module is fairly large, you could import dynamically:

```javascript
const module = await import('@techandsoftware/teletext-plugin-smooth-mosaic');
teletext.registerViewPlugin(module.SmoothMosaicPlugin);
```

## Non-ES6

If you want to support browsers that can't use ES6 modules, a UMD version is also available in the `dist` directory for importing with `<script>`. It exports the module in the global `ttxsmooth`.  It's likely you'll also need to transpile the code for your target browsers.

# Dependencies

* [js-hqx](https://www.npmjs.com/package/js-hqx), licensed under the [GNU Lesser General Public License, version 2.1](https://www.gnu.org/licenses/lgpl-2.1.en.html) or later.

# Credits

* The Ceefax page used in the image above was sourced from [Teletext Archaeologist](https://archive.teletextarchaeologist.org/)
