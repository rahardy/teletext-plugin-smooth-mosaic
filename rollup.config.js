// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
// SPDX-License-Identifier: ISC

import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from "rollup-plugin-terser";

// preamble in the minified source
let outputBanner = `// @preserve SPDX${''}-FileCopyrightText: (c) 2021 Tech and Software Ltd.
// @preserve SPDX${''}-FileCopyrightText: Copyright (C) 2017 Rohan Deshpande ( rohan@creativelifeform.com )
// @preserve SPDX${''}-FileCopyrightText: Copyright (C) 2010 Dominic Szablewski ( mail@phoboslab.org )
// @preserve SPDX${''}-FileCopyrightText: Copyright (C) 2010 Cameron Zemek ( grom@zeminvaders.net )
// @preserve SPDX${''}-FileCopyrightText: Copyright (C) 2003 Maxim Stepin ( maxst@hiend3d.com )
// @preserve SPDX${''}-License-Identifier: LGPL-2.1-or-later`;

export default {
  input: 'src/SmoothMosaicPlugin.js',
  output: [{
    file: 'dist/teletext-plugin-smooth-mosaic.js',
    format: 'es',
    banner: outputBanner
  }, {
    file: 'dist/teletext-plugin-smooth-mosaic.umd.js',
    format: 'umd',
    name: 'ttxsmooth',
    banner: outputBanner
  },{
    file: 'dist/teletext-plugin-smooth-mosaic.min.js',
    format: 'es',
    plugins: [terser({
      ecma: 2016,
      toplevel: true,
      compress: {
        drop_console: true,
        passes: 2,
      },
      format: {
        preamble: outputBanner
      }
    })]
  },{
    file: 'dist/teletext-plugin-smooth-mosaic.umd.min.js',
    format: 'umd',
    plugins: [terser({
      ecma: 2016,
      toplevel: true,
      compress: {
        drop_console: true,
        passes: 2,
      },
      format: {
        preamble: outputBanner
      }
    })],
    name: 'ttxsmooth',
  }],
  plugins: [
    resolve(),
    commonjs({
      include: ['node_modules/js-hqx/**', 'node_modules/imagetracerjs/**']
    }),
  ]
};
